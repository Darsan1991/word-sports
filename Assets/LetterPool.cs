﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LetterPool : MonoBehaviour, IInitializable<string[]>,IStartUpResolve
{
    public event Action<ILetterPoolTile> OnLetterPoolClicked;
    [SerializeField] private LetterPoolTile letterPoolTilePrefab;
    [SerializeField] private List<LetterPoolTile> letterPoolTilePool;
    [SerializeField] private List<LetterPoolHolder> letterPoolHolderPool;
    [SerializeField] private LetterPoolHolder letterPoolHolderPrefab;
    [SerializeField] private GridSizableLayout gridSizableLayout;
    [SerializeField] private Transform holderContentPanel;
    [SerializeField] private Transform tileContentPanel;
    [SerializeField] private MoveAnimScriptable defaultThrowAnim;
    [SerializeField] private AudioClip throwEffectClip;

    public Vector2 tileSize => gridSizableLayout.tileSize;
    public float tileSpace => gridSizableLayout.horizontalSpace;
    public bool inilized { get; private set; }
    public Game.IStadium stadium { get; set; }

    public int fixedColumnCount
    {
        get { return gridSizableLayout.fixedColumnCount; }
        set { gridSizableLayout.fixedColumnCount = value; }
    }

    private readonly List<LetterPoolTile> letterPoolTiles = new List<LetterPoolTile>();

    public void Init(string[] poolStrings)
    {
        if (inilized)
        {
            return;
        }

        if (!gridSizableLayout.inilized)
        {
             LateCall.Create().Call(()=>gridSizableLayout.inilized,()=>Init(poolStrings));
            return;
        }


        if (poolStrings != null && poolStrings.Length > 0)
        {
            var length = poolStrings[0].Length;
            foreach (var poolString in poolStrings)
            {
                if (poolString.Length != length)
                {
                    throw new Exception("Pool strings should be same size");
                }
            }


//            Debug.Log("Horizontal space:"+gridSizableLayout.horizontalSpace);

            while (letterPoolTilePool.Count < length * poolStrings.Length)
            {
                var letterPoolTile = Instantiate(letterPoolTilePrefab);
                letterPoolTile.transform.parent = tileContentPanel;
                letterPoolTilePool.Add(letterPoolTile);
            }

            while (letterPoolHolderPool.Count < length * poolStrings.Length)
            {
                var holder = Instantiate(letterPoolHolderPrefab);
                holder.transform.parent = holderContentPanel;

                letterPoolHolderPool.Add(holder);
            }

            letterPoolTiles.Clear();
//            Debug.Log("count:" + length * poolStrings.Length);
            for (var i = 0; i < letterPoolHolderPool.Count; i++)
            {
                if (length * poolStrings.Length > i)
                {
                    letterPoolHolderPool[i].gameObject.SetActive(true);
                    var letterPoolTile = letterPoolTilePool[i];
                    letterPoolTile.holder = letterPoolHolderPool[i];
                    letterPoolTile.letter = poolStrings[i / length][i % length].ToString();

                    letterPoolTiles.Add(letterPoolTile);
                }
                else
                {
                    letterPoolHolderPool[i].gameObject.SetActive(false);
                }
            }

            var availableLeftThrowPoints = new List<ThrowPoint>(stadium.leftThrowPoints);
            var availableRightThrowPoints = new List<ThrowPoint>(stadium.rightThrowPoints);

            for (var i = 0; i < letterPoolTilePool.Count; i++)
            {
                if (length * poolStrings.Length > i)
                {
                    var j = i % length;
                    if (j + 1 < length / 2)
                    {
                        var selectedPoint = Random.Range(0, availableLeftThrowPoints.Count);
                        letterPoolTilePool[i].transform.position =
                            availableLeftThrowPoints[selectedPoint].transform.position;

                        availableLeftThrowPoints.RemoveAt(selectedPoint);
                    }
                    else
                    {
                        var selectedPoint = Random.Range(0, availableRightThrowPoints.Count);

                        letterPoolTilePool[i].transform.position =
                            availableRightThrowPoints[selectedPoint].transform.position;
                        availableRightThrowPoints.RemoveAt(selectedPoint);
                    }
                    letterPoolTilePool[i].transform.localScale = Vector3.zero;
                    letterPoolTilePool[i].OnClicked += OnLetterPoolChildClicked;
                    letterPoolTilePool[i].letter = poolStrings[i / length][i % length].ToString();
//                    DebugUIManager.Show("LetterPoolTilePool size:"+letterPoolTilePool[i].transform.localScale+" pos"+letterPoolTilePool[i].transform.position);
                }
                else
                {
                    letterPoolTilePool[i].gameObject.SetActive(false);
                }
            }
            //            Debug.Log("length:" + length);
            //TODO:Try to remove later
            var worldWidth = Mathf.Abs((Camera.main.ScreenToWorldPoint(Vector3.zero) - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height))).x);
            var minTileSize = (worldWidth - gridSizableLayout.offset.left - gridSizableLayout.offset.right) / (length + (length - 1) * 0.1f);
            //            DebugUIManager.Show("World width:"+worldWidth);
            gridSizableLayout.horizontalSpace = Mathf.Min(gridSizableLayout.horizontalSpace, minTileSize / 10);
            gridSizableLayout.verticalSpace = gridSizableLayout.horizontalSpace;
            gridSizableLayout.fixedColumnCount = length;
        }



        inilized = true;
        foreach (var refreshable in GetComponentsInChildren<IRefreshable>())
        {
            refreshable.Refresh();
        }
//        DebugUIManager.Show("Tile size:"+tileSize);
        SendMessage(nameof(IRefreshable.Refresh), SendMessageOptions.DontRequireReceiver);
    }

    private void OnLetterPoolChildClicked(ILetterPoolTile tile)
    {
        if (tile.tileState == TileState.FREE)
            OnLetterPoolClicked?.Invoke(tile);
    }

    public void ThrowToStart(Action OnFinished = null)
    {
        StartCoroutine(ThrowToStartCor(OnFinished));
    }

    IEnumerator ThrowToStartCor(Action OnFinished = null)
    {
        var poolTiles = new List<LetterPoolTile>(letterPoolTiles);
        var movingTiles = new List<LetterPoolTile>(poolTiles);
        while (poolTiles.Count > 0)
        {
            var count = Random.Range(1, Mathf.Min(4, poolTiles.Count));
            for (var i = 0; i < count; i++)
            {
                var sel = Random.Range(0, poolTiles.Count);
                poolTiles[sel].MoveTo(poolTiles[sel].holder.transform.position, Vector3.one, defaultThrowAnim,
                    speedFactor: 1.2f);
                if (throwEffectClip != null)
                {
                    AudioManager.PlayIfPossible(throwEffectClip);
                }
                poolTiles.RemoveAt(sel);
            }

            yield return new WaitForSeconds(0.07f);
        }

        while (movingTiles.Any((m) => m.moving))
        {
//            DebugUIManager.Show("scale:"+movingTiles[0].transform.localScale.ToString()+" pos"+movingTiles[0].transform.position);
            yield return null;
        }
        OnFinished?.Invoke();
    }

    public ILetterPoolTile GetTile(char letter)
    {
        return letterPoolTiles.Find((t) => t.tileState == TileState.FREE && t.letter == letter.ToString());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            ThrowToStart();
        }
    }

    public void Resolve(Action OnComplete = null) => ThrowToStart(OnComplete);
}