﻿using System;

public interface ISplitableLetter:IUnityObject,ILetter
{
    IJoinableLetter joinableLetter { get; }
    void Split(Action OnFinished=null);
}


public interface IJoinableLetter : IUnityObject, ILetter
{
    ISplitableLetter splitableLetter { get; }
    void Join(ISplitableLetter tile, Action OnComplete = null);
}

public interface IJoinSplitableLetter : ISplitableLetter
{
    event Action<IJoinSplitableLetter> OnJoined;
}


public interface ILetter
{
    string letter { get; }
}

public interface ILetterPoolTile:IJoinSplitableLetter
{
    TileState tileState { get; }
    bool showingEffect { get; }
    event Action<ILetterPoolTile> OnTileStateChanged;
    void ShowEffect(bool success);
    void JoinTo(IJoinableLetter joinableLetter,Action OnFinished=null);
}

public interface IWordMakerTile : IJoinableLetter
{
    bool interactable { get; set; }
    bool showingEffect { get; }
    ILetterPoolTile letterPoolTile { get; }
    TileState state { get; }
    void ShowEffect(bool success);
    void Split(Action OnFinished=null);
}