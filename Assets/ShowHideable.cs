using System;
using UnityEngine;

public abstract class ShowHideable : MonoBehaviour,IShowHideable
{
    protected static int SHOW_HASH = Animator.StringToHash("Show");
    protected static int HIDE_HASH = Animator.StringToHash("Hide");

    [SerializeField] protected Animator anim;

    public bool showing { get { return gameObject.activeSelf; }protected set{gameObject.SetActive(value);}}

    protected abstract float showAnimTime { get; }
    protected abstract float hideAnimTime { get; }

    public virtual void Show(bool animate = true, Action OnFinished = null)
    {
        if (showing)
            return;

        showing = true;

        if (animate)
        {
//            anim.SetBool(SHOW_HASH, true);
            anim.Play(SHOW_HASH);
            var targetTime = Time.time + showAnimTime;
            LateCall.Create().Call(() => targetTime < Time.time, OnFinished);
        }
        else
        {
            OnFinished?.Invoke();
        }
    }

    public virtual void Hide(bool animate = true, Action OnFinished = null)
    {
        if (!showing)
            return;

        if (animate)
        {
            anim.Play(HIDE_HASH);
            var targetTime = Time.time + hideAnimTime;
            LateCall.Create().Call(() => targetTime < Time.time, () =>
            {
                showing = false;
                OnFinished?.Invoke();
            });
        }
        else
        {
            showing = false;
            OnFinished?.Invoke();
        }
    }
}