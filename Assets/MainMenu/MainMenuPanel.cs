using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainMenu
{
    public class MainMenuPanel : ShowHideable
    {
        [SerializeField] private AudioClip menuEnterClip, menuExitClip;

        protected override float showAnimTime => 0.5f;
        protected override float hideAnimTime => 0.5f;


        public void OnClickPlay()
        {
            Hide(OnFinished:GameManager.StartTheGame);
        }

        public void OnClickStore()
        {
            var storePanel = SharedUIManager.storePanel;
            storePanel.Show();
        }

        public void OnClickLeaderBoard()
        {
            SocialPlatformManager.instance.ShowLeadersboard();
        }

        public void OnClickSetting()
        {
            SharedUIManager.settingPanel.Show();
        }

        public void OnClickExit()
        {
            Hide(OnFinished:GameManager.Exit);
        }

        public override void Show(bool animate = true, Action OnFinished = null)
        {
            if(menuEnterClip!=null)
                AudioManager.PlayIfPossible(menuEnterClip);
            base.Show(animate, OnFinished);

        }

        public override void Hide(bool animate = true, Action OnFinished = null)
        {
            if (menuExitClip != null)
                AudioManager.PlayIfPossible(menuExitClip);
            base.Hide(animate, OnFinished);
        }

    }
}