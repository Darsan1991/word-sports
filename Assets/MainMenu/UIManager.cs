using System.Collections.Generic;
using UnityEngine;

namespace MainMenu
{
    public class UIManager : MonoBehaviour
    {
        private const string IS_FIRST_TIME_KEY = "first_time";
        public static UIManager instance { get; private set; }



        [SerializeField] private MainMenuPanel _mainMenuPanel;
        

        public MainMenuPanel mainMenuPanel => _mainMenuPanel;

        void Awake()
        {
            instance = this;
            if (PrefManager.GetBool(IS_FIRST_TIME_KEY,true))
            {
                var popUpPanel = SharedUIManager.popUpPanel;
                popUpPanel.viewModel = new PopUpPanel.ViewModel
                {
                    title = "Congratulations!",
                    message = "You Have 3 Blitzes, 3 Letters, & 3 Hints",
                    buttons = new List<PopUpPanel.ViewModel.Button> { new PopUpPanel.ViewModel.Button
                    {
                        title = "Ok"
                    }
                    }
                    
                };
                popUpPanel.active = true;
                PrefManager.AddConsumableVal(PrefManager.HINT_TYPE,3);
                PrefManager.AddConsumableVal(PrefManager.BLITZ_TYPE,3);
                PrefManager.AddConsumableVal(PrefManager.LETTER_TYPE,3);
                PrefManager.SetBool(IS_FIRST_TIME_KEY,false);
            }
        }

        void Start()
        {
            mainMenuPanel.Show();
        }

        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
                GameManager.Exit();
        }
    }
}