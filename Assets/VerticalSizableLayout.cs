using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class VerticalSizableLayout : DirectionSizableLayout
{
    protected override float height
    {
        get
        {
            var totalHeight = 0f;
            //total size of the tiles
            tileList.ForEach((t) => totalHeight += t.size.y);
            totalHeight += tileList.Count > 0 ? (tileList.Count - 1) * space : 0;
            return totalHeight;
        }
        
    }
        
//    => tileList.Count > 0 ? tileList.Max((val) => val.size.y) : 0;

    protected override float width=> tileList.Count > 0 ? tileList.Max((val) => val.size.x) : 0;


    protected override void UpdateChildLayout()
    {
        if (tileList.Count == 0)
            return;

        var targetPos = GetStartPositionForAlignment(alignment);
        for (var i = 0; i < tileList.Count; i++)
        {
            var position = tileList[i].transform.position;
            position.x = targetPos.x;
            position.y = targetPos.y;
            RectUtils.SetPositionWithPivot(tileList[i],position);
            if (i < tileList.Count - 1)
                targetPos.y -= tileList[i].size.y / 2 + tileList[i + 1].size.y / 2 + space;
        }
    }

    protected  Vector2 GetStartPositionForAlignment(Alignment alignment)
    {
        if (tileList.Count == 0)
            return Vector2.zero;

        var w = width;
        var h = height;
//        var contentWidth = 0;//w - tileList[0].size.x / 2 - tileList[tileList.Count - 1].size.x / 2;

        switch (alignment)
        {
            case Alignment.LOWER_LEFT:
                return new Vector2(transform.position.x + w / 2, transform.position.y + h-tileList[0].size.y/2);
            case Alignment.LOWER_MIDDLE:
                return new Vector2(transform.position.x, transform.position.y + h - tileList[0].size.y / 2);
            case Alignment.LOWER_RIGHT:
                return new Vector2(transform.position.x - w + tileList[0].size.x / 2, transform.position.y + h - tileList[0].size.y / 2);

            case Alignment.MIDDLE_LEFT:
                return new Vector2(transform.position.x + tileList[0].size.x / 2, transform.position.y+h/2 - tileList[0].size.y / 2);
            case Alignment.MIDDLE_CENTER:
                return new Vector2(transform.position.x, transform.position.y + h / 2 - tileList[0].size.y / 2
                    );
            case Alignment.MIDDLE_RIGHT:
                return new Vector2(transform.position.x - w + tileList[0].size.x / 2, transform.position.y + h / 2 - tileList[0].size.y / 2
                    );

            case Alignment.UPPER_LEFT:
                return new Vector2(transform.position.x + tileList[0].size.x / 2, transform.position.y - h + tileList[0].size.y/2);
            case Alignment.UPPER_CENTER:
                return new Vector2(transform.position.x, transform.position.y - h+ tileList[0].size.y / 2);
            case Alignment.UPPER_RIGHT:
                return new Vector2(transform.position.x - w + tileList[0].size.x / 2, transform.position.y - h + tileList[0].size.y / 2);
        }

        return Vector2.zero;
    }

}