using GooglePlayGames;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class SocialPlatformManager : MonoBehaviour
{
    private const string ANDROID_LEADERS_BOARD_ID = Constants.ANDROID_LEADERS_BOARD_ID;
    private const string IOS_LEADERS_BOARD_ID = Constants.IOS_LEADERS_BOARD_ID;

    public static SocialPlatformManager instance { get; private set; }

    public bool inilized { get; private set; }
    public bool isLogIn { get; private set; }


    public static string LeaderBoarderId
    {
        get
        {
#if UNITY_ANDROID
            return ANDROID_LEADERS_BOARD_ID;
#elif UNITY_IOS
            return IOS_LEADERS_BOARD_ID;
#endif
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        Init();
    }

    void Start()
    {
        SignIn();
    }

    void OnEnable()
    {
        PrefManager.OnBestScoreChanged += SubmitTopScoreOnLeadersBoard;
    }

    void OnDisable()
    {
        PrefManager.OnBestScoreChanged -= SubmitTopScoreOnLeadersBoard;
    }

    void Init()
    {
        if (inilized)
            return;
#if UNITY_ANDROID
        PlayGamesPlatform.Activate();
#endif
        inilized = true;
    }


    public void SignIn()
    {
        if (!inilized)
        {
            Init();
            return;
        }
        Social.localUser.Authenticate(success =>
        {
            // handle success or failure
            if (success)
            {
                isLogIn = true;
            }

        });

    }

    public void ShowLeadersboard()
    {
//        DebugUIManager.Show("Showing Leaderboard Login:"+isLogIn);

        if (!isLogIn)
        {
            SignIn();
            return;
        }
#if UNITY_ANDROID
//        PlayGamesPlatform.Instance.ShowLeaderboardUI(LeaderBoarderId);
                Social.ShowLeaderboardUI();
//        DebugUIManager.Show("Showing Leaderboard Login:" + isLogIn);

        //        GameCenterPlatform.ShowLeaderboardUI(LeaderBoarderId, TimeScope.AllTime);

#elif UNiTY_IOS
        UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowLeaderboardUI(LeaderBoarderId,TimeScope.AllTime);
#endif
    }

    public void SubmitTopScoreOnLeadersBoard(int score)
    {
        if (!isLogIn)
        {
            return;
        }
        Social.ReportScore(score, LeaderBoarderId, success =>
        {
            // handle success or failure
        });

    }

}