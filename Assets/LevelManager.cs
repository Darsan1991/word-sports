﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour, ILevelManager
{
    public const float MAX_GAME_PLAY_TIME = 30;

    public static LevelManager instance { get; private set; }
    public static event Action<State> OnStateChanged;

    public static event Action _OnLevelStart;
    public static event Action<bool> _OnLevelEnd;


    public event Action OnLevelStart;
    public event Action<bool> OnLevelEnd;
    public event Action<float> OnPlayTimeUpdated;



    [SerializeField] private LetterPool _letterPool;
    [SerializeField] private SentenceMaker _sentenceMaker;
    [SerializeField] private AudioClip timeUpClip;
    [SerializeField] private List<MonoBehaviour> _gameStartResolves = new List<MonoBehaviour>();

    public State currentState
    {
        get { return _currentState; }
        private set
        {
            if (_currentState!=value)
            {
                _currentState = value;
                OnStateChanged?.Invoke(currentState);
            }
        }
    } 

    public int currentLevel => gameHandler.currentLevel;

    private Queue<IStartUpResolve> resolvesBeforeGameStartQueue;

    public float playedTime
    {
        get { return _playedTime; }
        private set
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (_playedTime != value)
            {
                _playedTime = value;
                OnPlayTimeUpdated?.Invoke(playedTime);
            }
        }
    }

    public bool blazing { get; private set; }
    public Vector2 tileSize => letterPool.tileSize;
    public float tileSpace => letterPool.tileSpace;

    public LetterPool letterPool => _letterPool;

    public SentenceMaker sentenceMaker => _sentenceMaker;

    public int score => gameHandler.currentScore;
    public int coins => gameHandler.currentCoins;


    public IGameHandler gameHandler { get; private set; }

    public Model.IQuiz currentQuiz => gameHandler.currentQuiz;

    private PopUpPanel popUpPanel => SharedUIManager.popUpPanel;

    private float _playedTime;
    private State _currentState=State.NONE;


    void Awake()
    {
        instance = this;
        resolvesBeforeGameStartQueue = new Queue<IStartUpResolve>(_gameStartResolves.Select((r)=>(IStartUpResolve)r));
    }

    void Start()
    {

        gameHandler = GameManager.GameHandler;
        gameHandler.StartTheLevel(this);
        var targetWords = currentQuiz.answer.Split(' ');
        //       DebugUIManager.Show("Target words count:"+targetWords.Length+" first:"+targetWords[0]);
        letterPool.stadium = Instantiate(
            ResourceManager.GetStadiumForSports(currentQuiz.sportName)?.stadiumPrefab
            ??ResourceManager.GetStadiumForSports("FOOTBALL").stadiumPrefab,
            Vector3.zero, Quaternion.identity);
        letterPool.Init(GetPoolStringForTargetWords(targetWords, Mathf.Max(10, targetWords.Max((s) => s.Length))));
        LateCall.Create().Call(() => letterPool.inilized, () =>
        {
            sentenceMaker.Init(targetWords);
            Invoke(nameof(StartTheLevel), 0.5f);
        });

    }

    void StartTheLevel()
    {
        if (resolvesBeforeGameStartQueue.Count>0)
        {
            var resolve = resolvesBeforeGameStartQueue.Dequeue();
            resolve.Resolve(StartTheLevel);
            return;
        }

        playedTime = 0f;
        currentState = State.PLAYING;
        OnLevelStart?.Invoke();
        _OnLevelStart?.Invoke();
    }

    void EndTheLevel(bool completed)
    {
        if (currentState != State.PLAYING)
            throw new Exception("The Level is not playing");
        gameHandler.EndTheLevel(completed);
        currentState = State.FINISHED;

        OnLevelEnd?.Invoke(completed);
        _OnLevelEnd?.Invoke(completed);
    }

    void OnEnable()
    {
        letterPool.OnLetterPoolClicked += OnLetterPoolClicked;
        sentenceMaker.OnMakerStateChanged += OnSentenceMakerStateChanged;
    }

    void OnDisable()
    {
        letterPool.OnLetterPoolClicked -= OnLetterPoolClicked;
        sentenceMaker.OnMakerStateChanged -= OnSentenceMakerStateChanged;
    }

    private void OnSentenceMakerStateChanged(MakerState state)
    {
        if (currentState != State.PLAYING)
            return;
        if (state == MakerState.COMPLETED && sentenceMaker.correct)
        {
            EndTheLevel(true);
        }
    }

    private void OnLetterPoolClicked(ILetterPoolTile tile)
    {
        if (currentState != State.PLAYING)
            return;
        if (sentenceMaker.makerState == MakerState.NONE && !blazing)
        {
            var wordMakerTile = sentenceMaker.GetNextFreeTile();
            if (wordMakerTile != null)
                tile.JoinTo(wordMakerTile);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            Blitz();
        }
        if (currentState == State.PLAYING && !blazing)
        {
            var time = playedTime + Time.deltaTime;
            playedTime = Mathf.Clamp(time, 0, MAX_GAME_PLAY_TIME);
            if (playedTime >= MAX_GAME_PLAY_TIME)
            {
                if (sentenceMaker.makerState != MakerState.COMPLETED)
                {
                    if (timeUpClip != null)
                    {
                        AudioManager.PlayIfPossible(timeUpClip);
                    }
                }
                EndTheLevel(sentenceMaker.makerState == MakerState.COMPLETED
                            && sentenceMaker.correct);
            }
        }
    }

    public bool Blitz()
    {
        if(currentState != State.PLAYING)
            return false;
        blazing = true;
        StartCoroutine(BlitzCor(() => blazing = false));
        return true;
    }

    public void PauseTheGame()
    {
        if(currentState != State.PLAYING)
            return;

        currentState = State.PAUSED;
    }

    public void ResumeTheGame()
    {
        if(currentState!=State.PAUSED)
            return;
        currentState = State.PLAYING;
    }

    public bool ExposedLetter()
    {
        if (currentState != State.PLAYING)
        {
            return false;
        }
        var wordMakers = new List<WordMaker>(sentenceMaker.wordMakers);
        var makerIndex = wordMakers.FindIndex((w)=>w.makerState== MakerState.NONE);

        var strings = currentQuiz.answer.Split(' ');
        var hasFreeSpace = false;
        for (var i = 0; i < wordMakers.Count; i++)
        {
            var wordMaker = wordMakers[i];
            if (wordMaker.makerState == MakerState.NONE)
            {
                var wordMakerTiles = wordMaker.wordTiles.ToList();
                for (var j = 0; j < wordMakerTiles.Count; j++)
                {
                    if (wordMakerTiles[j].state == TileState.FREE)
                    {
                        
                        var poolTile = letterPool.GetTile(strings[i][j]);
                        hasFreeSpace = true;
                        if (poolTile != null)
                        {
                            wordMakerTiles[j].interactable = false;
                            poolTile.JoinTo(wordMakerTiles[j]);
                            
                            return true;
                        }
                    }
                }
            }
        }

        if (sentenceMaker.correct)
            return false;

        popUpPanel.viewModel = new PopUpPanel.ViewModel
        {
            title = "Cannot Expose Letter!",
            message = hasFreeSpace?
            "Letter in pool is not available for available free space.Try to add some free space"
            :"Free Space not available.Try to get some free space"
        };

        popUpPanel.active = true;

        return false;
    }

    IEnumerator BlitzCor(Action OnFinished = null)
    {
        var strings = currentQuiz.answer.Split(' ');

        var wordMakers = new List<WordMaker>(sentenceMaker.wordMakers);

        foreach (var wordMaker in wordMakers)
        {
            foreach (var wordTile in wordMaker.wordTiles)
            {
                if (wordTile.state == TileState.JOINED && !wordMaker.CorrectWordMakerTile(wordTile))
                {
                    var letterPoolTile = wordTile.letterPoolTile as LetterPoolTile;
                    if (letterPoolTile != null)
                    {
                        letterPoolTile.joinAnimType = JoinAnimType.CONSTANT_SPEED;
                    }
                    wordTile.Split();
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }

        for (var i = 0; i < wordMakers.Count; i++)
        {
            var wordMaker = wordMakers[i];
            var wordMakerTiles = new List<IWordMakerTile>(wordMaker.wordTiles);
            for (var j = 0; j < wordMakerTiles.Count; j++)
            {
                var wordMakerTile = wordMakerTiles[j];
                if (wordMakerTile.state != TileState.JOINED)
                {
                    ILetterPoolTile letterPoolTile;
                    while (true)
                    {
                        letterPoolTile = letterPool.GetTile(strings[i][j]);
                        if (letterPoolTile != null)
                            break;
                        yield return null;
                    }
                    var poolTile = letterPoolTile as LetterPoolTile;
                    if (poolTile != null)
                    {
                        poolTile.joinAnimType = JoinAnimType.CONSTANT_TIME;
                    }
                    letterPoolTile.JoinTo(wordMakerTile);
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }
        OnFinished?.Invoke();
    }

    public static string[] GetPoolStringForTargetWords(string[] targetWords, int lengthOfSinglePool = 10,
        int mimCount = 20)
    {
        var list = new List<char>();
        foreach (var targetWord in targetWords)
        {
            list.AddRange(targetWord);
        }

        var poolLength = Mathf.Clamp(Mathf.CeilToInt((list.Count + 0f) / lengthOfSinglePool),
            mimCount / lengthOfSinglePool, 1000);

        var poolCharacters = new char[poolLength][];
        for (var i = 0; i < poolCharacters.Length; i++)
        {
            poolCharacters[i] = new char[lengthOfSinglePool];
        }

        var indexList = new[] {new {x = 0, y = 0}}.ToList();
        indexList.Clear();

        for (var i = 0; i < poolLength; i++)
        {
            for (var j = 0; j < lengthOfSinglePool; j++)
            {
                indexList.Add(new {x = i, y = j});
            }
        }


        for (var i = 0; i < targetWords.Length; i++)
        {
            for (var j = 0; j < targetWords[i].Length; j++)
            {
                var selectedIndex = Random.Range(0, indexList.Count);
                var index = indexList[selectedIndex];
                indexList.RemoveAt(selectedIndex);
                poolCharacters[index.x][index.y] = targetWords[i][j];
            }
        }

        for (var i = 0; i < indexList.Count; i++)
        {
            poolCharacters[indexList[i].x][indexList[i].y] = (char) Random.Range(65, 91);
        }
        var poolStrings = new string[poolCharacters.Length];
        for (var i = 0; i < poolCharacters.Length; i++)
        {
            poolStrings[i] = poolCharacters[i].GetString().ToUpper();
        }
        return poolStrings;
    }

    void OnValidate()
    {
        for (var i = 0; i < _gameStartResolves.Count; i++)
        {
            var resolve = _gameStartResolves[i];
            if (!(resolve is IStartUpResolve))
            {
                resolve = resolve?.GetComponent<IStartUpResolve>() as MonoBehaviour;
                if (resolve==null)
                {
                    _gameStartResolves.RemoveAt(i);
                    i--;
                }
            }
        }
    }


    public enum State
    {
        NONE,
        PLAYING,
        PAUSED,
        FINISHED
    }
}

public interface IResolve
{
    void Resolve(Action OnComplete = null);
}

public interface IStartUpResolve : IResolve
{
    
}