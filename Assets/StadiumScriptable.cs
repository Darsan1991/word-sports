using System.Collections.Generic;
using System.Linq;
using Model;
using UnityEngine;

public class StadiumScriptable : ScriptableObject
{
    public const string DEFAULT_FILE_NAME = nameof(StadiumScriptable);

    [SerializeField] private Model.Stadium[] _stadiums;

    public IEnumerable<Model.IStadium> stadiums => _stadiums.Select((s)=>(Model.IStadium)s);


}