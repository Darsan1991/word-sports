public class Constants
{
    public static string AdmobIosInterstialId= "ca-app-pub-1414719774626316/2116088846";
    public static string AdmobAndroidInterstialId = "ca-app-pub-1414719774626316/5942942776";
    public static string AdmobIosBannerId= "ca-app-pub-1414719774626316/6647334569";
    public static string AdmobAndroidBannerId = "ca-app-pub-1414719774626316/472167";
    public static string IOS_APP_ID = "1305910353";

    public const string ANDROID_LEADERS_BOARD_ID= "CgkIpMv3s_0YEAIQAA";
    public const string IOS_LEADERS_BOARD_ID= "com.word.leadersboard";


#if UNITY_IOS
	public static string AppID = "app92f17aac8f5f4f0182";
	public static string InterstitialZoneID = "vzf5e36cb98fc54d2980";
	public static string CurrencyZoneID = "vzf5e36cb98fc54d2980";
#elif UNITY_ANDROID
    public static string AppID = "app2a119645a14b430eb4";
    public static string InterstitialZoneID = "vzac3c72bf3b76472482";
    public static string CurrencyZoneID = "vzac3c72bf3b76472482";
#else
	public static string AppID = "dummyAppID";
	public static string InterstitialZoneID = "dummyInterstitialZoneID";
	public static string CurrencyZoneID = "dummyCurrencyZoneID";
#endif
}