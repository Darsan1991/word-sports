﻿using System;
using System.Linq;
using Shop;
using UnityEngine;
using UnityEngine.UI;

public class StorePanel : MonoBehaviour,IShowHideable
{
    public event Action OnHide;
    private static readonly int SHOW_HASH = Animator.StringToHash("Show");
    private static readonly int HIDE_HASH = Animator.StringToHash("Hide");

    [SerializeField] private Animator anim;
    [SerializeField] private Button removeAdsBtn;

    public bool showing { get { return gameObject.activeSelf; } private set{gameObject.SetActive(value);} }

    //TODO:Move banner show hide to another script
    private bool bannerShowedBeforeOpen;

    public void Show(bool animate=true,Action OnComplete=null)
    {
        if(showing)
            return;
        bannerShowedBeforeOpen = AdsManager.BannerShowing;
        if(bannerShowedBeforeOpen)
        AdsManager.HideBanner();
        Refresh();
        showing = true;
        if (animate)
        {
//            anim.SetBool(SHOW_HASH, true);
            anim.Play(SHOW_HASH);
            var targetTime = Time.time + 0.6f;
            LateCall.Create().Call(()=>Time.time>targetTime, () =>
            {
                OnComplete?.Invoke();
            });
        }
        else
        {
            OnComplete?.Invoke();
        }
    }

    public void Hide(bool animate=true,Action OnComplete=null)
    {
        if (!showing)
            return;
       if(bannerShowedBeforeOpen)
            AdsManager.ShowBanner();

        if (animate)
        {
//            anim.SetBool(SHOW_HASH, false);
anim.Play(HIDE_HASH);
            var targetTime = Time.time + 0.6f;
            LateCall.Create().Call(() => Time.time > targetTime, () =>
            {
                showing = false;
                OnComplete?.Invoke();
                OnHide?.Invoke();
            });
        }
        else
        {
            showing = false;
            OnComplete?.Invoke();
            OnHide?.Invoke();
        }
    }

    public void OnClickRemoveAds()
    {
        var store = ResourceManager.Store;
        var featureItems = store.GetItem<IFeatureItem>();
        var removeAdsItem = featureItems.ToList().Find((item)=>item.featureItemType == FeatureItemType.REMOVE_ADS);

        ResourceManager.ConsumeOrUnlockItem(removeAdsItem, (item, success) =>
        {
            if(!success)
                return;
           PrefManager.SetPremiumVersion(true);
           Refresh();
        });
    }

    void Refresh()
    {
        removeAdsBtn.gameObject.SetActive(!PrefManager.IsPremiumVersion());
    }

    public void OnClickBackButton()
    {
        Hide();
    }
}

public interface IShowHideable
{
    bool showing { get; }
    void Show(bool animate = true, Action OnFinished = null);
    void Hide(bool animate = true, Action OnFinished = null);
}