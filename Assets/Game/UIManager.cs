﻿using System;
using UnityEngine;

namespace Game
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager instance { get; private set; }

        [SerializeField] private GamePlayPanel _gamePlayPanel;
        [SerializeField] private LevelCompletePanel _levelCompletePanel;
        [SerializeField] private LevelFailPanel _levelFailPanel;
        [SerializeField] private PausePanel _pausePanel;

        public PausePanel pausePanel => _pausePanel;
        public GamePlayPanel gamePlayPanel => _gamePlayPanel;
        public LevelFailPanel levelFailPanel => _levelFailPanel;
        public LevelCompletePanel levelCompletePanel => _levelCompletePanel;

        private LevelManager levelManager => LevelManager.instance;

        void Awake()
        {
            instance = this;
        }

        void OnEnable()
        {
            LevelManager._OnLevelEnd += OnLevelEnd;
        }

        void OnDisable()
        {
            LevelManager._OnLevelEnd -= OnLevelEnd;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameManager.GoToMainMenu();
            }
        }

        private void OnLevelEnd(bool success)
        {
            if (success)
            {
                Action OnEffectFinished = null;
                OnEffectFinished = () =>
                {
                    levelManager.sentenceMaker.OnSuccessEffectFinished -= OnEffectFinished;
                    levelCompletePanel.viewModel = new LevelCompletePanel.ViewModel
                    {
                        level = levelManager.currentLevel,
                        score = levelManager.score,
                        coins = levelManager.coins
                    };
                    levelCompletePanel.active = true;
                };
                levelManager.sentenceMaker.OnSuccessEffectFinished += OnEffectFinished;

            }
            else
            {
                var targetTime = Time.time + 0.1f;
                LateCall.Create().Call(() => Time.time > targetTime, () =>
                {
                    levelFailPanel.viewModel = new LevelFailPanel.ViewModel
                    {
                        level = levelManager.currentLevel,
                        score = levelManager.score,
                        coins = levelManager.coins,
                        bestScore = Mathf.Max(PrefManager.GetHighScore(),levelManager.score)
                       
                    };
                    levelFailPanel.active = true;
                    PrefManager.AddConsumableVal(PrefManager.COINS_TYPE,levelManager.coins);
                });
            }
        }
    }
}