using System;
using UnityEngine;

public class RectableObject : MonoBehaviour,IRectUnityObject
{
    [SerializeField] private Vector2 normalizedSize;
    [SerializeField] private Vector2 _pivot;
    public event Action<IRectable> OnSizeChanged;

    public Vector2 size
    {
        get
        {
            return new Vector2(normalizedSize.x*transform.localScale.x,normalizedSize.y*transform.localScale.y);
        }
        set
        {
            var localScale = transform.localScale;
            localScale.x = value.x / normalizedSize.x;
            localScale.y = value.y / normalizedSize.y;
            transform.localScale = localScale;
        }
    }

    public Vector2 pivot => _pivot;

}