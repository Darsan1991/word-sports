﻿using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class HorizontalSizableLayout : DirectionSizableLayout
{

    protected override float height => tileList.Count > 0 ? tileList.Max((val) => val.size.y):0;

    protected override float width
    {
        get
        {
            var totalWith = 0f;
            //total size of the tiles
            tileList.ForEach((t) => totalWith += t.size.x);
            totalWith += tileList.Count > 0 ? (tileList.Count - 1) * space : 0;
            return totalWith;
        }
    }


    


    protected override void UpdateChildLayout()
    {
        if (tileList.Count == 0)
            return;
//        Debug.Log("Height:"+height);
        var targetPos = GetStartPositionForAlignment(alignment);
//        Debug.Log("Target Start Point:"+targetPos);
        for (var i = 0; i < tileList.Count; i++)
        {
            var position = tileList[i].transform.position;
            position.x = targetPos.x;
            position.y = targetPos.y;
            RectUtils.SetPositionWithPivot(tileList[i],position);
            if (i < tileList.Count - 1)
                targetPos.x += tileList[i].size.x / 2 + tileList[i + 1].size.x / 2 + space;
        }
    }

    protected  Vector2 GetStartPositionForAlignment(Alignment alignment)
    {
        if (tileList.Count == 0)
            return Vector2.zero;

        var w = width;
        var h = height;
        var contentWidth = w - tileList[0].size.x / 2 - tileList[tileList.Count - 1].size.x / 2;

        switch (alignment)
        {
            case Alignment.LOWER_LEFT:
                return new Vector2(transform.position.x + tileList[0].size.x / 2, transform.position.y + h/ 2);
            case Alignment.LOWER_MIDDLE:
                return new Vector2(transform.position.x -contentWidth/2, transform.position.y + h / 2);

//                return new Vector2(transform.position.x - contentWidth / 2, transform.position.y + h / 2);
            case Alignment.LOWER_RIGHT:
                return new Vector2(transform.position.x - w +tileList[0].size.x/2, transform.position.y + h / 2);


            case Alignment.MIDDLE_LEFT:
                return new Vector2(transform.position.x + tileList[0].size.x / 2, transform.position.y);

            case Alignment.MIDDLE_CENTER:
                return new Vector2(transform.position.x - contentWidth / 2, transform.position.y);
            case Alignment.MIDDLE_RIGHT:
                return new Vector2(transform.position.x - w + tileList[0].size.x / 2, transform.position.y);

            case Alignment.UPPER_LEFT:
                return new Vector2(transform.position.x + tileList[0].size.x / 2, transform.position.y - h / 2);
            case Alignment.UPPER_CENTER:
                return new Vector2(transform.position.x - contentWidth / 2, transform.position.y - h / 2);
            case Alignment.UPPER_RIGHT:
                return new Vector2(transform.position.x - w+tileList[0].size.x/2 , transform.position.y - h / 2);
        }

        return Vector2.zero;
    }


}