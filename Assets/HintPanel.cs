﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class HintPanel : MonoBehaviour
{
    public const float HINT_SHOW_TIME = 5f;
    public event Action OnHide;
    public static readonly int SHOW_HASH = Animator.StringToHash("Show");

    [SerializeField] private Animator anim;
    [SerializeField] private Text hintTxt;

    public string hint { get { return hintTxt.text; } set { hintTxt.text = value; } }

    public bool showing { get { return gameObject.activeSelf; }private set { gameObject.SetActive(value);} }

    public void Show()
    {
        showing = true;
        anim.SetBool(SHOW_HASH,true);
        
    }

    public void Hide(Action OnFinished=null)
    {
        anim.SetBool(SHOW_HASH,false);
        var targetTime = Time.time + 1f;
        LateCall.Create().Call(()=>Time.time>targetTime, () =>
        {
            showing = false;
            OnFinished?.Invoke();
        });
    }
}