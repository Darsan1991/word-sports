﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Model;
using UnityEngine;

namespace Model
{
    public interface IStadium
    {
        string sportName { get; }
        Game.Stadium stadiumPrefab { get; }
    }

    [System.Serializable]
    public struct Stadium:IStadium
    {
        [SerializeField]private  string _sportName;
        [SerializeField]private  Game.Stadium _stadiumPrefab;

        public string sportName => _sportName;

        public Game.Stadium stadiumPrefab => _stadiumPrefab;
    }
}

public static class CoroutineUtils
{
   public static IEnumerator Lerp(Action<float> OnUpdate, Action OnFinished=null, AnimationCurve curve = null, bool reverseCurve = false, float speedFactor = 1f, float tolerance = 0.01f)
    {
        var currentNormalized = 0f;

        while (currentNormalized < 1f - tolerance)
        {
            var speed = curve?.Evaluate(reverseCurve ? 1 - currentNormalized : currentNormalized) ?? 1f;
            currentNormalized = Mathf.MoveTowards(currentNormalized, 1, Time.deltaTime * speed * speedFactor);
            OnUpdate?.Invoke(currentNormalized);
            yield return null;
        }

        OnFinished?.Invoke();
    }
}

public static class DataUtils
{
    public static class QuizUtils
    {
        public static string ToJson(IEnumerable<IQuiz> quizzes)
        {
            var list = quizzes.Select((q) => new Quiz(q.sportName, q.answer, q.hint)).ToList();
            var quizData = new QuizData
            {
                data = list
            };
            return JsonUtility.ToJson(quizData);
        }

        public static IEnumerable<IQuiz> FromJson(string json)
        {
            if (string.IsNullOrEmpty(json))
                return null;
            var quizData = JsonUtility.FromJson<QuizData>(json);
            return quizData.data.Select((q) => (IQuiz)q);
        }

        public struct QuizData
        {
            public List<Quiz> data;
        }
    }

}