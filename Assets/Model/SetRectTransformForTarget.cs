using UnityEngine;

//assume target anchor is upper left
public class SetRectTransformForTarget : MonoBehaviour
{
    [SerializeField] private RectTransform targetRectTransform;

    private RectTransform rectTransform => (RectTransform) transform;

    void Update()
    {
        var anchoredPosition = targetRectTransform.anchoredPosition;
        var transformAnchoredPosition = rectTransform.anchoredPosition;
        transformAnchoredPosition.y = anchoredPosition.y;
        rectTransform.anchoredPosition = transformAnchoredPosition;
//            = anchoredPosition;
        rectTransform.sizeDelta = targetRectTransform.sizeDelta;
    }
}