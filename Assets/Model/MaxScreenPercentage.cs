﻿using UnityEngine;

public class MaxScreenPercentage : MonoBehaviour
{
    [SerializeField] private float maxScreenPercentage;
    private RectTransform rectTransform => (RectTransform) transform;

    private RectTransform rootRectTransform => (RectTransform) transform.root;

    void Start()
    {
        var percentage = rectTransform.rect.size.y/rootRectTransform.rect.size.y;
        if (percentage > maxScreenPercentage)
        {
            var height = rootRectTransform.rect.size.y * maxScreenPercentage;
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x,height);
        }
        var anchoredPosition = rectTransform.anchoredPosition;
        anchoredPosition.y = 0f;
        rectTransform.anchoredPosition = anchoredPosition;
    }


}