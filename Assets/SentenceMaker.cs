using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SentenceMaker : MonoBehaviour,IParentObject,IInitializable<string[]>
{
    public event Action OnSuccessEffectFinished;
    public event Action<MakerState> OnMakerStateChanged;

    [SerializeField] private WordMaker wordMakerPrefab;
    [SerializeField] private List<WordMaker> wordMakerPool;
    [SerializeField] private AudioClip successClip,failureClip;

    public string[] targetWords { get;private set; }
    public bool inilized { get; private set; }

    private bool ableToVibrate => PrefManager.GetToggleSetting(ToggleSetting.VIBRATION);


    public MakerState makerState
    {
        get { return _makerState; }
        private set
        {
            if (_makerState!=value)
            {
                _makerState = value;
                OnMakerStateChanged?.Invoke(makerState);
            }
        }
    }

    public bool correct { get { return wordMakerLists.All((w) => w.correctWord); } }

    public bool interaction
    {
        get
        {
            return _interaction;
        }
        set
        {
            if(interaction==value)
                return;
            _interaction = value;
        }
    }

    public IEnumerable<WordMaker> wordMakers => wordMakerLists;


    private readonly List<WordMaker> wordMakerLists = new List<WordMaker>();
    private bool _completed;
    private bool _interaction=true;
    private MakerState _makerState;


    public void Init(string[] targetWords)
    {
        if (inilized)
        {
            return;
        }
        
        this.targetWords = targetWords;
        while (wordMakerPool.Count<targetWords.Length)
        {
            var wordMaker = Instantiate(wordMakerPrefab);
            wordMaker.transform.parent = transform;
            wordMakerPool.Add(wordMaker);
        }

        for (var i = 0; i < wordMakerPool.Count; i++)
        {
            var wordMaker = wordMakerPool[i];
            wordMaker.parent = this;
            if (i < targetWords.Length)
            {
                
                wordMaker.Init(targetWords[i]);
                wordMaker.OnWordCompleteStateChanged += OnWordMakeWordComplete;
                wordMaker.gameObject.SetActive(true);
                wordMakerLists.Add(wordMaker);
            }
            else
            {
                wordMaker.gameObject.SetActive(false);
            }
        }

        inilized = true;
        SendMessage(nameof(IRefreshable.Refresh),SendMessageOptions.DontRequireReceiver);
    }

    public IWordMakerTile GetNextFreeTile()
    {
        if (makerState != MakerState.NONE)
            return null;
        var wordMaker = wordMakerLists.Find((w)=>w.makerState == MakerState.NONE);
        return wordMaker.GetNextFreeTile();
    }

    private void OnWordMakeWordComplete(WordMaker maker)
    {

        for (var i = 0; i < wordMakerLists.Count; i++)
        {
            if (wordMakerLists[i].makerState == MakerState.NONE)
            {
                makerState = MakerState.NONE;
                break;
            }
            if (i == wordMakerLists.Count - 1)
            {
                makerState = wordMakerLists[i].makerState;
            }
        }

        if (makerState == MakerState.COMPLETED)
        {
            if (correct)
            {
                interaction = false;
                StartCoroutine(ShowSuccessEffect(()=>OnSuccessEffectFinished?.Invoke()));
            }
            else
            {
                ShowFailedEffect();
            }
        }
    }

    void ShowFailedEffect()
    {
        if (ableToVibrate)
        {
            Handheld.Vibrate();
        }
        if(failureClip!=null)
        AudioManager.PlayIfPossible(failureClip);
        var list = new List<IWordMakerTile>();
        wordMakerLists.ForEach((w) => list.AddRange(w.wordTiles));
        for (var i = 0; i < list.Count; i++)
        {
            list[i].ShowEffect(false);
        }
    }

    IEnumerator ShowSuccessEffect(Action OnComplete=null)
    {
        if(successClip!=null)
            AudioManager.PlayIfPossible(successClip);
        var list = new List<IWordMakerTile>();
        wordMakerLists.ForEach((w)=>list.AddRange(w.wordTiles));
        for (var i = 0; i < list.Count; i++)
        {
            list[i].ShowEffect(true);
            yield return new WaitForSeconds(0.05f);
        }

        while (list.Any((e)=>e.showingEffect))
        {
            yield return null;
        }
        OnComplete?.Invoke();
    }


}

public interface IParentObject : IUnityObject
{
    bool interaction { get; }
}

public interface IChildObject : IUnityObject
{
    IParentObject parent { get; set; }
}

