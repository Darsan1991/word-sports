using System;
using UnityEngine;
using UnityEngine.EventSystems;


public class LetterPoolTile : MonoBehaviour, ILetterPoolTile
{
    public event Action<ILetterPoolTile> OnClicked;

    public event Action<IJoinSplitableLetter> OnJoined;
    public event Action<ILetterPoolTile> OnTileStateChanged;

    [SerializeField] private ColliderButton colliderButton;

    [SerializeField] private AudioClip joinClip, splitClip;
//    [SerializeField] private AnimationCurve textColorCurve;
//    [SerializeField] private AnimationCurve successScaleCurve;
    [SerializeField] private TextMesh text;

    public IJoinableLetter joinableLetter { get; private set; }
    
    public TileState tileState
    {
        get
        {
            return _tileState;
        }
        private set
        {
            if (_tileState!=value)
            {
                _tileState = value;
                OnTileStateChanged?.Invoke(this);
                if (tileState == TileState.JOINED)
                {
                    OnJoined?.Invoke(this);
                }
            }
        }
    }

    public JoinAnimType joinAnimType { get; set; } = JoinAnimType.CONSTANT_SPEED;

    public string letter
    {
        get { return text.text; }
        set { text.text = value; }
    }

    public ILetterPoolHolder holder
    {
        get { return _holder; }
        set
        {
            if (_holder != value)
            {
                if (_holder != null)
                {
                    _holder.OnSizeChanged -= OnHolderSizeChanged;
                    _holder.OnEnableEvent -= OnHolderEnable;
                    _holder.OnDisableEvent -= OnHolderDisable;
                }
                _holder = value;

                if (_holder != null)
                {
                    _holder.OnSizeChanged += OnHolderSizeChanged;
                    _holder.OnEnableEvent += OnHolderEnable;
                    _holder.OnDisableEvent += OnHolderDisable;
                    gameObject.SetActive(_holder.gameObject.activeSelf);
                }
            }
        }
    }

    public bool moving { get; private set; }

    public bool interaction
    {
        get { return colliderButton.interaction; }
        set { colliderButton.interaction = value; }
    }

    public bool showingEffect { get; private set; }

    private ILetterPoolHolder _holder;
    private TileState _tileState;


    public void ShowEffect(bool success)
    {
        showingEffect = true;

        if (success)
        {
            ScaleLerp(Vector3.one * 1.3f, OnFinished: () =>
                ScaleLerp(Vector3.one*1.02f,speedFactor:10
//                ,curve:successScaleCurve
                ,reverseCurve:true,OnFinished:()=>showingEffect=false),speedFactor:10
//                ,curve:successScaleCurve
            );
        }
        else
        {
            TextColorLerp(Color.red, OnFinished: () => TextColorLerp(Color.white,speedFactor:4
//                ,curve:textColorCurve
                ,reverseCurve:true, OnFinished: () => showingEffect = false),speedFactor:4f
//                ,curve:textColorCurve
                );
        }
    }




    void TextColorLerp(Color targetColor, AnimationCurve curve = null, bool reverseCurve = false,
        float speedFactor = 1f, Action OnFinished = null)
    {
        var startColor = text.color;
        StartCoroutine(CoroutineUtils.Lerp(OnUpdate: f => text.color = Color.Lerp(startColor, targetColor, f),
            OnFinished: () =>
            {
                text.color = targetColor;
                OnFinished?.Invoke();
            }, curve: curve, reverseCurve: reverseCurve, speedFactor: speedFactor, tolerance: 0f));
    }

    void ScaleLerp(Vector3 targetScale, AnimationCurve curve = null, bool reverseCurve = false, float speedFactor = 1f,
        Action OnFinished = null)
    {
        var startScale = transform.localScale;
        StartCoroutine(CoroutineUtils.Lerp(
            OnUpdate: f => transform.localScale = Vector3.Lerp(startScale, targetScale, f), OnFinished: () =>
            {
                transform.localScale = targetScale;
                OnFinished?.Invoke();
            }, curve: curve, reverseCurve: reverseCurve, speedFactor: speedFactor, tolerance: 0f));
    }

    public void MoveTo(Vector3 targetPosition, Vector3 targetScale, IMoveAnim moveAnim = null, float speedFactor = 1,
        Action OnFinished = null)
    {
        moving = true;
        var offset = -Vector3.forward * 8;
        var startPos = transform.position + offset;
        targetPosition = targetPosition + offset;
        var startScale = transform.localScale;

        StartCoroutine(CoroutineUtils.Lerp(f =>
        {
            transform.position = Vector3.Lerp(startPos, targetPosition, f);
            float scale = moveAnim?.scaleAnimationCurve.Evaluate(f) ?? f;
            transform.localScale = (targetScale - startScale) * scale + startScale;
        }, () =>
        {
            transform.position = targetPosition - offset;
            moving = false;
            OnFinished?.Invoke();
        }, curve: moveAnim?.speedAnimationCurve, reverseCurve: false, speedFactor: speedFactor, tolerance: 0));
    }


    private void OnHolderEnable(IUnityEvent obj)
    {
        gameObject.SetActive(true);
    }

    private void OnHolderDisable(IUnityEvent obj)
    {
        gameObject.SetActive(false);
    }

    private void OnHolderSizeChanged(IRectable rect)
    {
//        Debug.Log(nameof(OnHolderSizeChanged));
        GetComponent<IRectUnityObject>().size = rect.size;
    }


    public void Split(Action OnFinished=null)
    {
        if(splitClip!=null)
            AudioManager.PlayIfPossible(splitClip);
        if (joinableLetter != null)
        {
            joinableLetter = null;
        }
        transform.localScale *= 1.08f;
        MoveTo(holder.transform.position, Vector3.one, OnFinished: () =>
            {
                tileState = TileState.FREE;
                interaction = true;
                OnFinished?.Invoke();
            },
            speedFactor: joinAnimType == JoinAnimType.CONSTANT_SPEED?10 / (holder.transform.position - transform.position).magnitude
                : 5);
    }

    public void OnClickDown()
    {
        if(EventSystem.current.IsPointerOverGameObject(
#if !UNITY_EDITOR
            Input.GetTouch(0).fingerId
#endif
            ))
            return;
        if (moving)
            return;
        OnClicked?.Invoke(this);
    }

    public void JoinTo(IJoinableLetter joinableLetter,Action OnFinished=null)
    {
        if(joinClip!=null)
            AudioManager.PlayIfPossible(joinClip);
        transform.localScale = transform.localScale * 1.1f;
        this.joinableLetter = joinableLetter;
        tileState = TileState.JOINING;
        this.joinableLetter.Join(this);
        interaction = false;
        MoveTo(joinableLetter.transform.position, Vector3.one*1.02f,
            speedFactor: joinAnimType == JoinAnimType.CONSTANT_SPEED?
            10 / (joinableLetter.transform.position - transform.position).magnitude
            :5
            ,OnFinished: () =>
            {
                tileState = TileState.JOINED;
                OnFinished?.Invoke();
            });
    }
}

public enum JoinAnimType
{
    CONSTANT_SPEED,CONSTANT_TIME
}

namespace MainMenu
{
}