using System;
using System.Collections.Generic;
using System.Linq;
using Shop;
using UnityEngine;

public class ConsumableProvider : MonoBehaviour, IConsumableProvider
{
    [SerializeField] private ConsumableProductType consumeType;

    public IEnumerable<IConsumableItem> consumableItems
    {
        get
        {
            var store = ResourceManager.Store;
            switch (consumeType)
            {
                case ConsumableProductType.HINT:
                    return store.GetItem<IHintItem>().Select((c) => (IConsumableItem) c);
                case ConsumableProductType.BLITZ:
                    return store.GetItem<IBlitzItem>().Select((c) => (IConsumableItem) c);

                case ConsumableProductType.LETTER:
                    return store.GetItem<ILetterItem>().Select((c) => (IConsumableItem) c);

                case ConsumableProductType.COIN:
                    return store.GetItem<ICoinItem>().Select((c) => (IConsumableItem) c);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public enum ConsumableProductType
    {
        HINT,
        BLITZ,
        LETTER,
        COIN
    }
}