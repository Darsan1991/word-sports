using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanel : ShowHideable,IInitializable
{

    [SerializeField] private Toggle musicToggle;
    [SerializeField] private Toggle soundToggle;
    [SerializeField] private Toggle vibrationToggle;

    protected override float showAnimTime => 0.4f;
    protected override float hideAnimTime => 0.4f;


    public bool hasSound
    {
        get
        {
            return PrefManager.GetToggleSetting(ToggleSetting.SOUND);
        }
        private set
        {
            PrefManager.SetToggleSetting(ToggleSetting.SOUND, value);
            soundToggle.isOn = value;
        }
    }

    public bool hasMusic
    {
        get
        {
            return PrefManager.GetToggleSetting(ToggleSetting.MUSIC);
        }
        private set
        {
            PrefManager.SetToggleSetting(ToggleSetting.MUSIC, value);
            musicToggle.isOn = value;
        }
    }
    public bool hasVibration
    {
        get
        {
            return PrefManager.GetToggleSetting(ToggleSetting.VIBRATION);
        }
        private set
        {
            PrefManager.SetToggleSetting(ToggleSetting.VIBRATION, value);
            vibrationToggle.isOn = value;
        }
    }

    public bool inilized { get; private set; }
    public void Init()
    {
        if (inilized)
        {
            return;
        }
        musicToggle.onValueChanged.AddListener((val => hasMusic=val));
        soundToggle.onValueChanged.AddListener((val => hasSound=val));
        vibrationToggle.onValueChanged.AddListener((val =>
        {
            hasVibration = val;
            Debug.Log("Vibration toggle");
        }));
        inilized = true;
    }

    void Awake()
    {
        Init();
    }

    public override void Show(bool animate = true, Action OnFinished = null)
    {
        Refresh();
        base.Show(animate, OnFinished);
    }

    public void OnClickCloseOrBack()
    {
        Hide();
    }

    void Refresh()
    {
        hasSound = PrefManager.GetToggleSetting(ToggleSetting.SOUND);
        hasMusic = PrefManager.GetToggleSetting(ToggleSetting.MUSIC);
        hasVibration = PrefManager.GetToggleSetting(ToggleSetting.VIBRATION);
    }
}