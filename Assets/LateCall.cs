using System;
using System.Collections;
using UnityEngine;

public class LateCall : MonoBehaviour
{
    public delegate bool Condition();

    public void Call(Condition condition, Action onFinished)
    {
        StartCoroutine(CallAfterConditionCor(condition, () =>
        {
            onFinished?.Invoke();
            Destroy(gameObject);
        }));
    }

    IEnumerator CallAfterConditionCor(Condition condition, Action OnFinished)
    {
        if (condition == null)
            yield break;
        while (!condition())
        {
            yield return null;
        }
        OnFinished?.Invoke();
    }
    public static LateCall Create()
    {
        var go = new GameObject("LateCall");
        return go.AddComponent<LateCall>();
    }
}