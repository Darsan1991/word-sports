﻿using UnityEngine;

public static class Extensions
{
    public static Vector3 ToRealScale(this Transform transform)
    {
        var scale = Vector3.one;
        var currentTrans = transform;
        while (currentTrans!=null)
        {
            var currentTransLocalScale = currentTrans.localScale;
            scale = new Vector3(scale.x*currentTransLocalScale.x,scale.y*currentTransLocalScale.y,scale.z*currentTransLocalScale.z);
            currentTrans = currentTrans.parent;
        }

        return scale;

    }

    public static string GetString(this char[] characters)
    {
        var str = "";
        for (var i = 0; i < characters.Length; i++)
        {
            str += characters[i].ToString();
        }
        return str;
    }

    public static string ToShortString(this int val)
    {
        if (val >= 1000000)
        {
            return (val%1000000==0?(val/1000000).ToString():$"{val / 1000000f:F1}")+"m";
        }

        if (val >= 1000)
        {

            return (val % 1000 == 0?(val/1000).ToString():$"{val / 1000f:F1}")+"k";
        }

        return val.ToString();
    }
}