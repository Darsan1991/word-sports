﻿using Shop;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SplashScreen : MonoBehaviour
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private ScaleType scaleType;

    public RectTransform rectTransform => (RectTransform) transform;

    private float spriteAspect => sprite.bounds.extents.y / sprite.bounds.extents.x;

    void Awake()
    {
        var image = GetComponent<Image>();
        image.sprite = sprite;

        if (scaleType == ScaleType.SCALE_TO_FILL)
        {
            var width = rectTransform.rect.size.x;
            var height = rectTransform.rect.size.y;

            var aspect = (Screen.height + 0f) / Screen.width;

            var scale = (spriteAspect > aspect)?
                Screen.width/width: Screen.height / height;
            transform.localScale = Vector3.one*scale;
        }
//        PrefManager.AddConsumableVal<IBlitzItem>(100);
        LateCall.Create().Call(()=>ResourceManager.Inilized, () =>
        {
            GameManager.LoadScene("MainMenu");
        });
    }



    public enum ScaleType
    {
        SCALE_TO_FILL
    }
}