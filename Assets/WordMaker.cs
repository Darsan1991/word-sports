using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WordMaker : MonoBehaviour,IInitializable<string>,IParentObject,IChildObject
{
    public event Action<WordMaker> OnWordCompleteStateChanged;

    [SerializeField] private WordMakerTile wordMakerTilePrefab;
    [SerializeField] private List<WordMakerTile> worMakerTilesPool;

    #region Properties

    public string targetWord { get; private set; }
    public bool inilized { get; private set; }

    public MakerState makerState
    {
        get { return _makerState; }
        private set
        {
            if (_makerState!=value)
            {
                _makerState = value;
                OnWordCompleteStateChanged?.Invoke(this);
            }
        }
    }

    public bool correctWord
    {
        get
        {
            if (makerState != MakerState.COMPLETED) return false;

            return !wordMakerTileList
                .Where((t, i) => 
                    !string.Equals(
                        targetWord[i].ToString()
                        , t.letter
                        , StringComparison.CurrentCultureIgnoreCase))
                .Any();
        }
    }

    public bool interaction
    {
        get
        {
            return _interaction&&parent.interaction;
        }
        set
        {
            _interaction = value;
        }
    }

    public IParentObject parent { get; set; }

    public IEnumerable<IWordMakerTile> wordTiles => wordMakerTileList;

    #endregion

    #region Fields

    private readonly List<WordMakerTile> wordMakerTileList = new List<WordMakerTile>();
    private bool _interaction=true;
    private MakerState _makerState;

    #endregion

    public void Init(string targetWord)
    {
        if (inilized)
        {
            return;
        }
        this.targetWord = targetWord;

        if(wordMakerTileList.Count>0)
            wordMakerTileList.Clear();

        //make ready the pool
        while (targetWord.Length>worMakerTilesPool.Count)
        {
            var wordMakerTile = Instantiate(wordMakerTilePrefab);
            wordMakerTile.transform.parent = transform;
            worMakerTilesPool.Add(wordMakerTile);
        }

        for (var i = 0; i < worMakerTilesPool.Count; i++)
        {
            var wordMakerTile = worMakerTilesPool[i];
            
            if (i < targetWord.Length)
            {
                wordMakerTile.parent = this;
                wordMakerTile.gameObject.SetActive(true);
                wordMakerTileList.Add(wordMakerTile);
                wordMakerTile.OnWordTileStateChanged += OnWordTileStateChanged;
               
            }
            else
            {
                wordMakerTile.gameObject.SetActive(false);
            }
        }

        inilized = true;
    }

    public bool CorrectWordMakerTile(IWordMakerTile tile)
    {
        var index = wordMakerTileList.FindIndex((t) => (IWordMakerTile)t == tile);
       return tile.state == TileState.JOINED && String.Equals(wordMakerTileList[index].letterPoolTile.letter, targetWord[index].ToString(), StringComparison.CurrentCultureIgnoreCase);
    }

    public IWordMakerTile GetNextFreeTile()
    {
        if (makerState == MakerState.COMPLETED || makerState == MakerState.COMPLETING)
        {
            return null;
        }
        return wordMakerTileList.Find((t) => t.state == TileState.FREE);
    }

    private void OnWordTileStateChanged(IWordMakerTile wordMakerTile, TileState wordTileState)
    {
        for (var i = 0; i < wordMakerTileList.Count; i++)
        {
            if (wordMakerTileList[i].state == TileState.FREE)
            {
                makerState = MakerState.NONE;
                break;
            }
            if (i == wordMakerTileList.Count - 1)
            {
                makerState = wordMakerTileList[i].state == TileState.JOINING ? MakerState.COMPLETING : MakerState.COMPLETED;
            }
        }
    }
}