using System.IO;
using UnityEngine;

public static class GeneralButtonUtils
{
    private const string SCREEN_SHOT_NAME = "screenshot.png";

    public static void ShareScreenshotWithText(string text)
    {
        string screenShotPath = Application.persistentDataPath + "/" + SCREEN_SHOT_NAME;
        if (File.Exists(screenShotPath)) File.Delete(screenShotPath);

        ScreenCapture.CaptureScreenshot(SCREEN_SHOT_NAME);

        LateCall.Create().Call((() => File.Exists(screenShotPath)), () =>
        {
            NativeShare.Share(text, screenShotPath, "", "", "image/png", true, "");
        });

    }
}