using UnityEngine;

public static class RectUtils
{
    public static void SetPositionWithPivot(IRectUnityObject rect,Vector2 targetMiddlePosition)
    {
        var rectSize = rect.size;
        var pivot = rect.pivot;
        var xOffset = rectSize.x*(pivot.x-0.5f);
        var yOffset = rectSize.y*(pivot.y-0.5f);
        rect.transform.position = targetMiddlePosition + new Vector2(xOffset, yOffset);
    }

    public static void DrawGizmos(this IRectUnityObject rectUnityObject,Color? color =null)
    {
        Gizmos.color = color ?? Color.white;
        var pivot = rectUnityObject.pivot;
        var size = rectUnityObject.size;
        var transform = rectUnityObject.transform;

        var xOffset = -size.x * (pivot.x - 0.5f);
        var yOffset = -size.y * (pivot.y - 0.5f);

        var middlePos = transform.position + new Vector3(xOffset, yOffset);

        Gizmos.DrawLine(middlePos + new Vector3(-size.x/2,-size.y/2), middlePos + new Vector3(size.x / 2, -size.y / 2));
        Gizmos.DrawLine(middlePos + new Vector3(-size.x/2,+size.y/2), middlePos + new Vector3(size.x / 2, +size.y / 2));
        Gizmos.DrawLine(middlePos + new Vector3(-size.x/2,+size.y/2), middlePos + new Vector3(-size.x / 2, -size.y / 2));
        Gizmos.DrawLine(middlePos + new Vector3(size.x / 2, -size.y / 2), middlePos + new Vector3(size.x / 2, size.y / 2));
    }

    
}