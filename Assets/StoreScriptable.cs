using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Shop
{
    public class StoreScriptable : ScriptableObject, IStore
    {
        public static readonly Type HINT_TYPE = typeof(IHintItem);
        public static readonly Type BLITZ_TYPE = typeof(IBlitzItem);
        public static readonly Type LETTER_TYPE = typeof(ILetterItem);
        public static readonly Type COIN_TYPE = typeof(ICoinItem);

        public const string DEFAULT_FILE_NAME = nameof(StoreScriptable);

        [SerializeField] private HintItemOfCoin[] _hints;
        [SerializeField] private BlitzItemOfCoin[] _blitzes;
        [SerializeField] private LetterItemOfCoin[] _letters;
        [SerializeField] private CoinItemOfPremium[] _coins;
        [SerializeField] private FeatureItem removeAdsItem;

        public IEnumerable<T> GetItem<T>() where T:IItem
        {
            return GetAllItems().OfType<T>();
        }

        public IEnumerable<IItem> GetAllItems()
        {
            var consumableItems = new List<IItem>();
            consumableItems.AddRange(_hints);
            consumableItems.AddRange(_blitzes);
            consumableItems.AddRange(_coins);
            consumableItems.AddRange(_letters);
            consumableItems.Add(removeAdsItem);
            return consumableItems;
        }

        public IEnumerable<IItem> GetItem(Type t)
        {
            return GetAllItems().Where(t.IsInstanceOfType);
        }
    }


}