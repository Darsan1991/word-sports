﻿using System;
using System.Collections.Generic;

namespace Shop
{
    public interface IStore
    {
        IEnumerable<IItem> GetAllItems();
        IEnumerable<T> GetItem<T>() where T : IItem;
        IEnumerable<IItem> GetItem(Type t);
    }
}