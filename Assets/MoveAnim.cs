﻿using UnityEngine;
[System.Serializable]
public struct MoveAnim:IMoveAnim
{
    [SerializeField] private AnimationCurve _speedAnimationCurve;
    [SerializeField] private AnimationCurve _scaleAnimationCurve;
    [SerializeField] private AnimationCurve _pathAnimationCurve;

    public AnimationCurve speedAnimationCurve => _speedAnimationCurve;
    public AnimationCurve scaleAnimationCurve => _scaleAnimationCurve;
    public AnimationCurve pathAnimationCurve => _pathAnimationCurve;
}

public interface IMoveAnim
{
    AnimationCurve speedAnimationCurve { get; }
    AnimationCurve scaleAnimationCurve { get; }
    AnimationCurve pathAnimationCurve { get; }
}