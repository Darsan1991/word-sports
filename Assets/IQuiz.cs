using Model;
using UnityEngine;

namespace Model
{
    

public interface IQuiz
{
    string sportName { get; }
    string answer { get; }
    string hint { get; }
}

}

[System.Serializable]
public struct Quiz:IQuiz
{
    [SerializeField]private string _sportName;
    [SerializeField] string _answer;
    [SerializeField] string _hint;

    public string sportName => _sportName;
    public string answer => _answer;
    public string hint => _hint;

    public Quiz(string sportName, string answer, string hint)
    {
        _sportName = sportName;
        _answer = answer;
        _hint = hint;
    }
}