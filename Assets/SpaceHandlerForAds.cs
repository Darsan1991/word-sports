using System.Collections.Generic;
using UnityEngine;


public class SpaceHandlerForAds:MonoBehaviour
{
    [SerializeField] private float offSetOfUI;
    [SerializeField] private float worldOffset;

    [SerializeField] private RectTransform gamePlayPanel;
    [SerializeField] private Transform[] globalTransformsNeedToMove = new Transform[0];


    private bool showingBanner => AdsManager.BannerShowing;

    private readonly Dictionary<Transform,Vector3> transformVsOriginalPositionsDict = new Dictionary<Transform, Vector3>();

    void Awake()
    {
        foreach (var t in globalTransformsNeedToMove)
        {
            if (!transformVsOriginalPositionsDict.ContainsKey(t))
            {
                transformVsOriginalPositionsDict.Add(t,t.position);
            }
        }
        if(!showingBanner && AdsManager.BannerLoaded)
            AdsManager.ShowBanner();
        RefreshForAds();
    }


    void RefreshForAds()
    {
        gamePlayPanel.offsetMin = new Vector2(0,showingBanner?offSetOfUI:0);

        foreach (var t in globalTransformsNeedToMove)
        {
            t.position = transformVsOriginalPositionsDict[t] + Vector3.up * (showingBanner?worldOffset:0);
        }
    }
}