using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class ColliderButton : MonoBehaviour
{
    [SerializeField]private UnityEvent _onClick;

    public bool interaction { get { return mCollider.enabled; }set { mCollider.enabled = value; } }

    private Collider mCollider
    {
        get
        {
            if (_mCollider == null)
                _mCollider = GetComponent<Collider>();
            return _mCollider;
        }
    }

    public UnityEvent onClick => _onClick;

    private Collider _mCollider;

    void OnMouseDown()
    {
        if(interaction)
            onClick?.Invoke();
        
    }
}