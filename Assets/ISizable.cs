﻿using System;
using UnityEngine;

public interface IRectable
{
    event Action<IRectable> OnSizeChanged; 
    Vector2 size { get; set; }
    Vector2 pivot { get;  }
}
