using UnityEngine;
using UnityEngine.EventSystems;

public class PlayAtOnClick : MonoBehaviour,IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        AudioManager.PlayIfPossible(AudioManager.ON_CLICK_CLIP);
    }
}