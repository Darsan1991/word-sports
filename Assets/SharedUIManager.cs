﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SharedUIManager : MonoBehaviour
{
    public static SharedUIManager instance { get; private set; }

    [SerializeField] private LoadingPanel _loadingPanel;
    [SerializeField] private PopUpPanel _popUpPanel;
    [SerializeField] private StorePanel _storePanel;
    [SerializeField] private SettingPanel _settingPanel;

    public static LoadingPanel loadingPanel => instance._loadingPanel;
    public static PopUpPanel popUpPanel => instance._popUpPanel;
    public static StorePanel storePanel => instance._storePanel;
    public static SettingPanel settingPanel => instance._settingPanel;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

}