﻿using System;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[ExecuteInEditMode]
public class SpriteResizer : MonoBehaviour,IRectUnityObject
{
    public event Action<IRectable> OnSizeChanged;

    
    [SerializeField] private float width;
    [SerializeField] private float height;
    [SerializeField] private bool keepAspectSer;

    /// <summary>
    /// size of the sprite in world size rel to parent
    /// the changed value will be considered when aspect ratio enabled other one will be reject
    /// x will be ger high priority when changed.
    /// </summary>
    public Vector2 size
    {
        get { return _size; }
        set
        {
            if(size==value)
                return;
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            bool xChanged = size.x != value.x;
//            Debug.Log("Set Size:"+transform.parent.name);
            _size = value;

            if (keepAspect)
            {
                if (xChanged)
                    _size.y = size.x * aspectRatio;
                else _size.x = size.y / aspectRatio;
            }
            SetSize(size);
            OnSizeChanged?.Invoke(this);
        }
    }

    public Vector2 pivot => //sprite?.pivot ??
        Vector2.one*0.5f;

//    public float width
//    {
//        get { return _width; }
//        set
//        {
//            // ReSharper disable once CompareOfFloatsByEqualityOperator
//            if(_width!=value)
//            SetSize(value,keepAspect?value*aspectRatio:height);
//        }
//    }

    public float aspectRatio => spriteRenderer.sprite == null
        ? 1
        : spriteRenderer.sprite.textureRect.height / spriteRenderer.sprite.textureRect.width;

//    public float height
//    {
//        get { return _height; }
//        set
//        {
//            // ReSharper disable once CompareOfFloatsByEqualityOperator
//            if(_height!=value)
//            SetSize(keepAspect ? (value / aspectRatio): width, value);
//        }
//    }

    public bool keepAspect
    {
        get { return _keepAspect; }
        set
        {

            _keepAspect = value;
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (keepAspect&&size.y / size.x != aspectRatio)
            {
                var s = size;
                s.y = size.x * aspectRatio;
                size = s;
            }
        }
    }

    private SpriteRenderer spriteRenderer { get
    {
        if (_spriteRenderer==null)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }
        return _spriteRenderer;
    } }

    private Sprite sprite
    {
        get {
            if (_sprite == null)
            {
                sprite = spriteRenderer.sprite;
            }
            return
                _sprite; }
        set
        {
            if (_sprite!=value)
            {
                _sprite = value;
                SetSize(size);
            }
        }
    }

    private bool _keepAspect;


    private SpriteRenderer _spriteRenderer;
    private Sprite _sprite;
    private Vector2 _size = Vector2.one*0.5f;

    void Awake()
    {
//        OnValidate();
        if (width <= 0)
            width = 0.00001f;
        if (height <= 0)
            height = 0.00001f;

//                if (size.x != width)
//                {
//                    var s = size;
//                    s.x = width;
//                    size = s;
//                }
//        
//                // ReSharper disable once CompareOfFloatsByEqualityOperator
//                if (size.y!=height)
//                {
//                    var s = size;
//                    s.y = height;
//                    size = s;
//                }

        if (keepAspect != keepAspectSer)
        {
            keepAspect = keepAspectSer;
        }

        if (sprite != spriteRenderer.sprite)
        {
            sprite = spriteRenderer.sprite;
        }
        //        sprite = spriteRenderer.sprite;
        //        size = new Vector2(width,height);
        //        keepAspect = keepAspectSer;
    }

    void SetSize(Vector2 size)
    {
        if (sprite != null)
        {
            transform.localScale = new Vector3(size.x/(2 *sprite.bounds.extents.x), size.y / (2 * sprite.bounds.extents.y),1);
        }
        width = size.x;
        height = size.y;
    }


    void OnValidate()
    {
        if (width <= 0)
            width = 0.00001f;
        if (height <= 0)
            height = 0.00001f;
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (size.x != width)
        {
            var s = size;
            s.x = width;
            size = s;
        }

        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (size.y!=height)
        {
            var s = size;
            s.y = height;
            size = s;
        }

        if (keepAspect != keepAspectSer)
        {
            keepAspect = keepAspectSer;
        }

        if (sprite != spriteRenderer.sprite)
        {
            sprite = spriteRenderer.sprite;
        }
    }
}