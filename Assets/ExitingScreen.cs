﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ExitingScreen : MonoBehaviour
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private ScaleType scaleType;
    [SerializeField] private List<GameObject> nonDestroyObjectAtExit;

    public RectTransform rectTransform => (RectTransform)transform;

    private float spriteAspect => sprite.bounds.extents.y / sprite.bounds.extents.x;

    void Awake()
    {
//        var image = GetComponent<Image>();
//        image.sprite = sprite;
//
//        if (scaleType == ScaleType.SCALE_TO_FILL)
//        {
//            var width = rectTransform.rect.size.x;
//            var height = rectTransform.rect.size.y;
//
//            var aspect = (Screen.height + 0f) / Screen.width;
//
//            var scale = (spriteAspect > aspect) ?
//                Screen.width / width : Screen.height / height;
//            transform.localScale = Vector3.one * scale;
//        }
        
    }

    IEnumerator Start()
    {
        yield return null;
        var gos = FindObjectsOfType<GameObject>();
        for (var i = 0; i < gos.Length; i++)
        {
            if (!nonDestroyObjectAtExit.Contains(gos[i]))
            {
                DestroyImmediate(gos[i]);
            }
        }
        yield return null;
        Resources.UnloadUnusedAssets();
        yield return null;
        GC.Collect();
        yield return null;
        Application.Quit();
    }



    public enum ScaleType
    {
        SCALE_TO_FILL
    }
}