using Shop;
using UnityEditor;
using UnityEngine;

public static class MenuExtension
{
    public const string STADIUM_SCRIPTABLE_PATH = "Assets/Resources/" + StadiumScriptable.DEFAULT_FILE_NAME + ".asset";
    public const string PRODUCT_SCRIPTABLE_PATH = "Assets/Resources/" + StoreScriptable.DEFAULT_FILE_NAME + ".asset";

    [MenuItem("My Games/Stadium Settings")]
    public static void OpenStadiumSetting()
    {
        var stadiumScriptable = AssetDatabase.LoadAssetAtPath<StadiumScriptable>(STADIUM_SCRIPTABLE_PATH);
        if (stadiumScriptable == null)
        {
            stadiumScriptable = ScriptableObject.CreateInstance<StadiumScriptable>();
            AssetDatabase.CreateAsset(stadiumScriptable,STADIUM_SCRIPTABLE_PATH);
            AssetDatabase.SaveAssets();
        }
        Selection.activeObject = stadiumScriptable;
    }

    [MenuItem("My Games/Store Settings")]
    public static void OpenProductSetting()
    {
        var storeScriptable = AssetDatabase.LoadAssetAtPath<StoreScriptable>(PRODUCT_SCRIPTABLE_PATH);
        if (storeScriptable == null)
        {
            storeScriptable = ScriptableObject.CreateInstance<StoreScriptable>();
            AssetDatabase.CreateAsset(storeScriptable, PRODUCT_SCRIPTABLE_PATH);
            AssetDatabase.SaveAssets();
        }
        Selection.activeObject = storeScriptable;
    }

    [MenuItem("My Games/Clear Prefs")]
    public static void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}