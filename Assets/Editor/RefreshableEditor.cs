using UnityEditor;
using UnityEngine;

public class RefreshableEditor : Editor
{
    protected IRefreshable refreshable => target as IRefreshable;

    //Call the DrawRefresh to 
    public override void OnInspectorGUI()
    {
        DrawTopInspector();
        DrawRefresh();
    }

    protected virtual void DrawTopInspector()
    {
        base.OnInspectorGUI();
    }

    protected void DrawRefresh()
    {
//        Debug.Log("DrawRefresh");
        if (GUILayout.Button("Refresh"))
        {
            refreshable?.Refresh();
        }
    }
}