using System;
using UnityEngine;
using UnityEngine.EventSystems;


public class WordMakerTile : MonoBehaviour,IWordMakerTile,IChildObject
{
    public event Action<IWordMakerTile> OnClicked; 
    public event Action<IWordMakerTile,TileState> OnWordTileStateChanged;


    [SerializeField] private ColliderButton colliderButton;

    public ILetterPoolTile letterPoolTile
    {
        get { return (ILetterPoolTile) splitableLetter; }
        private set { splitableLetter = value; }
    }

    public ISplitableLetter splitableLetter { get; private set; }
    //IJoinSplitableLetter IJoinableLetter.letterPoolTile => letterPoolTile;

   
    public TileState state { get; private set; } = TileState.FREE;

    public bool showingEffect => letterPoolTile?.showingEffect??false;

    public bool interactable
    {
        get
        {
            return colliderButton.interaction&&parent.interaction;
        }
        set { colliderButton.interaction = value; }
    }

    public IParentObject parent { get; set; }

    public string letter => letterPoolTile?.letter;

    void Start()
    {
        if(LevelManager.instance!=null)
        GetComponent<IRectUnityObject>().size = LevelManager.instance.tileSize;
    }

    public void ShowEffect(bool success)
    {
        letterPoolTile?.ShowEffect(success);
    }



    public void Join(ISplitableLetter tile,Action OnFinished=null)
    {
        //TODO:run coroututine
        state = TileState.JOINING;
        splitableLetter = tile ;
        Action<IJoinSplitableLetter> act = null;
        act = letter =>
        {
            letterPoolTile.OnJoined -= act;
            state = TileState.JOINED;
            OnWordTileStateChanged?.Invoke(this,state);
        };
        letterPoolTile.OnJoined += act;
    }

    public void OnClickDown()
    {
        if (EventSystem.current.IsPointerOverGameObject(
#if !UNITY_EDITOR
            Input.GetTouch(0).fingerId
#endif
        ))
            return;
        if (interactable)
        {
            Split();
        }

    }

    public void Split(Action OnFinished=null)
    {
        letterPoolTile?.Split();
        letterPoolTile = null;
        state = TileState.FREE;
        OnWordTileStateChanged?.Invoke(this, state);
        OnClicked?.Invoke(this);
        OnFinished?.Invoke();
    }
}

public enum MakerState
{
    NONE,COMPLETING,COMPLETED
}

public enum TileState
{
    FREE, JOINING, JOINED
}