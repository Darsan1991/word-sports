using System;
using UnityEngine;

public class PausePanel : ShowHideable
{

    protected override float showAnimTime => 1f;
    protected override float hideAnimTime => 0.2f;

    public void OnClickSetting()
    {
        SharedUIManager.settingPanel.Show();
    }

    public void OnClickHome()
    {
        Hide(OnFinished:GameManager.GoToMainMenu);
        
    }

    public void OnClickContinue()
    {
        Hide(OnFinished:LevelManager.instance.ResumeTheGame);
        
    }

    public void OnClickCloseOrBack()
    {
        Hide();
    }

}