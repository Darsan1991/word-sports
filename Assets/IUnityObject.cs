using UnityEngine;

public interface IUnityObject
{
    GameObject gameObject { get; }
    Transform transform { get; }
}