using System;
using System.Collections.Generic;
using AdColony;
using UnityEngine;
using GoogleMobileAds.Api;
using InterstitialAd = GoogleMobileAds.Api.InterstitialAd;

public class AdsManager : MonoBehaviour,IInitializable
{
    public static event Action<VideoAdsResult> OnVideoAdsFinished;

    public const int MIN_GAME_OVER_BETWEEN_ADS = 2;


    public static AdsManager instance { get; private set; }

    public static bool Inilized => instance.inilized;
    public static bool BannerLoaded => instance.bannerLoaded;
    public static bool BannerShowing => instance.bannerShowing;

    public bool inilized { get;private set; }

    public bool bannerLoaded { get; private set; }
    public bool bannerShowing { get; private set; }

    private int gameOversLeftForAds;
    private bool nextInterstialAds;
    private BannerView bannerView;
    private InterstitialAd interstitial;
    private AdColony.InterstitialAd adColonyInterstitialAd;

    private bool premiumVersion => PrefManager.IsPremiumVersion();


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        Init();
    }

    void OnEnable()
    {
        LevelManager._OnLevelEnd += OnLevelEnd;
        
    }

    void OnDisable()
    {
        LevelManager._OnLevelEnd -= OnLevelEnd;
    }

    private void OnLevelEnd(bool success)
    {
        if(premiumVersion)
            return;
        gameOversLeftForAds--;
        if (gameOversLeftForAds <= 0)
        {
            if (nextInterstialAds)
            {
                if (ShowInterstial())
                {
                    gameOversLeftForAds = MIN_GAME_OVER_BETWEEN_ADS;
                    nextInterstialAds = false;
                }
            }
            else
            {

                ShowVideoAds((result) =>
                {
                    if (result == VideoAdsResult.SUCCESS)
                    {
                        gameOversLeftForAds = MIN_GAME_OVER_BETWEEN_ADS;
                        nextInterstialAds = true;
                    }
                });
            }

        }
    }

    #region Static Functions


    public static void ShowBanner()
    {
        if(!instance.bannerLoaded)
            return;
        instance.bannerView.Show();
        instance.bannerShowing = true;
    }

    public static void HideBanner()
    {
        instance.bannerView.Hide();
        instance.bannerShowing = false;
    }

    public static bool ShowInterstial()
    {
        Debug.Log(nameof(ShowInterstial));
        var interstitialAd = instance.interstitial;
        if (interstitialAd.IsLoaded())
        {
            interstitialAd.Show();
            return true;
        }
        return false;
    }

    public static void ShowVideoAds(Action<VideoAdsResult> OnFinished = null)
    {
        Debug.Log(nameof(ShowVideoAds));
        if (instance.adColonyInterstitialAd != null)
        {
            Action<VideoAdsResult> VideoFinished=null;
            VideoFinished = result =>
            {
                OnVideoAdsFinished -= VideoFinished;
                OnFinished?.Invoke(result);
            };
            OnVideoAdsFinished += VideoFinished;
            Ads.ShowAd(instance.adColonyInterstitialAd);
        }
    }


    #endregion



    public void Init()
    {

        if (inilized)
            return;
        gameOversLeftForAds = MIN_GAME_OVER_BETWEEN_ADS;
        RequestBanner();
        RequestInterstitial();
        interstitial.OnAdClosed += (sender, args) => {
            var request = new AdRequest.Builder().Build();
            interstitial.LoadAd(request);
        };
        bannerView.OnAdLoaded += (sender, args) => bannerLoaded = true;
        bannerView.Hide();
        nextInterstialAds = true;
        Action<List<Zone>> OnConfigure = null;
        OnConfigure = (list) =>
        {
            Ads.OnConfigurationCompleted -= OnConfigure;
            RequestAdColonyAd();
        };
        Ads.OnConfigurationCompleted += OnConfigure;
        Ads.OnClosed += ad =>
        {
            OnVideoAdsFinished?.Invoke(VideoAdsResult.SUCCESS);
            RequestAdColonyAd();
        };
        Ads.OnRequestInterstitial += ad => { adColonyInterstitialAd = ad; };
        ConfigureAds();
        inilized = true;
    }

    void ConfigureAds()
    {
        var appOptions = new AppOptions();
        string[] zoneIDs = { Constants.InterstitialZoneID, Constants.CurrencyZoneID };
        Ads.Configure(Constants.AppID, appOptions, zoneIDs);
    }





    #region Requests

    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = Constants.AdmobAndroidBannerId;
#elif UNITY_IPHONE
        string adUnitId = Constants.AdmobIosBannerId;
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = Constants.AdmobAndroidInterstialId;
            //"ca-app-pub-9090942574524127/6688407495";
#elif UNITY_IPHONE
        string adUnitId = Constants.AdmobIosInterstialId;
        //"ca-app-pub-9090942574524127/2118607092";
#else
        string adUnitId = "unexpected_platform";
#endif


        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    void RequestAdColonyAd()
    {
        var adOptions = new AdOptions();
        Ads.RequestInterstitialAd(Constants.CurrencyZoneID, adOptions);
    }

    #endregion



    public enum VideoAdsResult
    {
        SUCCESS,FAILED
    }

}