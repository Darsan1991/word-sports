﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class LevelCompletePanel : ShowHideable
    {
        [SerializeField] private Text levelTxt;
        [SerializeField] private Text scoreTxt;
        [SerializeField] private Text coinTxt;
        [SerializeField] private AudioClip nextLevelClip;

        public ViewModel viewModel
        {
            get
            {
                return _viewModel;
            }
            set
            {
                _viewModel = value;
                levelTxt.text =  value.level+"";
                coinTxt.text = viewModel.coins.ToString();
                scoreTxt.text = viewModel.score.ToString();
            }
        }

        public bool active
        {
            get
            {
                return showing;
            }
            set
            {
                if(value)
                    Show();
                else
                {
                    Hide();
                }
            }
        }

        protected override float showAnimTime => 0.4f;
        protected override float hideAnimTime => 0.4f;

        private ViewModel _viewModel;


        public void OnClickContinueBtn()
        {
            if(nextLevelClip!=null)
            AudioManager.PlayIfPossible(nextLevelClip);
            Hide(OnFinished:GameManager.ContinueTheGame);
        }

        public void OnClickHomeButton()
        {
            Hide(OnFinished:GameManager.GoToMainMenu);
        }

        public void OnClickShareButton()
        {
            GeneralButtonUtils.ShareScreenshotWithText("Hey! Check this awesome game.\n" +
#if UNITY_ANDROID
                                                       "https://play.google.com/store/apps/details?id="+Application.identifier
#elif UNITY_IOS
                                                       "https://itunes.apple.com/us/app/keynote/id"+ Constants.IOS_APP_ID +"?mt=8"
#endif
                                                       );
        }

        public struct ViewModel
        {
            public int level { get; set; }
            public int  score { get; set; }
            public int  coins { get; set; }
        }
    }
}