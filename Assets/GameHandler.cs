using System;
using System.Collections.Generic;
using Model;

public class GameHandler : IGameHandler
{
    public readonly IEnumerable<IQuiz> quizzes;

    public event Action OnStartTheLevel;
    public event Action<bool> OnEndTheLevel;
    
    private readonly IGameResultCalculator gameResultCalculator;
    public int currentScore { get; private set; } = 0;
    public int currentCoins { get; private set; } = 0;
    public int currentLevel { get; private set; } = 0;

    public bool playing => currentState == State.PLAYING || currentState==State.COMPLETED;

    public ILevelManager levelManager
    {
        get;
        private set;
    }

    public State currentState { get; private set; } = State.NONE;
 
    public IQuiz currentQuiz { get; private set; }

    private int nextLevel = 1;

    private readonly List<IQuiz> availableQuiz;


    public void EndTheLevel(bool success)
    {
        if(currentState!=State.PLAYING)
            throw new Exception("The Level is not playing");
        currentState = success ? State.COMPLETED : State.FAILED;
        if (success)
        {
            var completedTime = levelManager.playedTime;
            int s;
            int c;
            gameResultCalculator.CalculateResult(completedTime, currentLevel,out s,out c);
            currentScore += s;
            currentCoins += c;
            nextLevel = currentLevel + 1;
        }
        OnEndTheLevel?.Invoke(success);
    }

    public void StartTheLevel(ILevelManager levelManager)
    {
        if(currentState==State.PLAYING)
            throw new Exception("The Level is already started");
        this.levelManager = levelManager;
        currentLevel = nextLevel;
        var selectedIndex = UnityEngine.Random.Range(0, availableQuiz.Count);
        currentQuiz = availableQuiz[selectedIndex];
        availableQuiz.RemoveAt(selectedIndex);
        currentState = State.PLAYING;
        OnStartTheLevel?.Invoke();

    }

    public GameHandler(IGameResultCalculator scoreCalculator,IEnumerable<IQuiz> quizzes)
    {
        
        this.gameResultCalculator = scoreCalculator;
        this.quizzes = quizzes;
        this.availableQuiz = new List<IQuiz>(this.quizzes);
    }


    public enum State
    {
        NONE,PLAYING,COMPLETED,FAILED
    }

}

public interface IGameResultCalculator
{
    void CalculateResult(float completedTime, int currentLevel, out int score, out int coins);
}

namespace MainMenu
{
}