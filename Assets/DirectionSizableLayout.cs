using UnityEngine;

public abstract class DirectionSizableLayout : SizableLayout
{
    [SerializeField] protected float spaceSer;

    public float space
    {
        get { return _space; }
        set
        {
            _space = value;
            UpdateChildLayout();
        }
    }


    private float _space;

    protected override void OnValidate()
    {
        base.OnValidate();
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        // ReSharper disable once RedundantCheckBeforeAssignment
        if (spaceSer != space)
        {
            space = spaceSer;
        }
    }
}