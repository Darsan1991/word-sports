using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DebugUIManager : MonoBehaviour
{
    public static DebugUIManager instance { get; private set; }

    public delegate bool TestDelegate();

//    public event TestDelegate testDelegate;

    [SerializeField] private Text text;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
//        testDelegate += () => false;
//        testDelegate += () => true;
//
//
//        Debug.Log(testDelegate?.Invoke());
    }

    public static void Show(string text, float time = 10f)
    {
        instance.text.text = text;
        instance.text.gameObject.SetActive(true);
        instance.CancelInvoke(nameof(Hide));
        instance.Invoke(nameof(Hide),time);
    }

    public static void Hide() => instance.text.gameObject.SetActive(false);

    public void Exit()
    {
        Application.Quit();
    }


    public void LoadGame()
    {
        SceneManager.LoadScene("LoadingScene");
    }
}