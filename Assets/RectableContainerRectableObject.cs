using System;
using UnityEngine;

public class RectableContainerRectableObject : MonoBehaviour,IRectUnityObject
{
    [SerializeField] private MonoBehaviour _rectableChild;

    public event Action<IRectable> OnSizeChanged
    {
        add { rectableChild.OnSizeChanged += value; }
        remove { rectableChild.OnSizeChanged -= value; }
    }
    public Vector2 size { get { return rectableChild.size; } set { rectableChild.size = value; } }
    public Vector2 pivot => rectableChild.pivot;

    public IRectUnityObject rectableChild=>_rectableChild as IRectUnityObject;


    void OnValidate()
    {
        if (!(_rectableChild is IRectUnityObject))
        {
            _rectableChild = null;
        }
    }

}