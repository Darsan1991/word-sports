using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Stadium : MonoBehaviour, IStadium
    {
        [SerializeField] private ThrowPoint[] _leftThrowPoints;
        [SerializeField] private ThrowPoint[] _rightThrowPoints;
        [SerializeField] private Vector2 minAndMaxAspectRatio;
        [SerializeField] private AudioSource audioSource;

        public IEnumerable<ThrowPoint> leftThrowPoints => _leftThrowPoints;
        public IEnumerable<ThrowPoint> rightThrowPoints => _rightThrowPoints;

        private IAudioController audioPlayer;

        void Awake()
        {
            var aspect = (Screen.height + 0f) / Screen.width;
            var clamp = Mathf.Clamp(aspect, minAndMaxAspectRatio.x, minAndMaxAspectRatio.y);

            transform.localScale = Vector3.one * clamp / aspect;
            audioPlayer = new AudioPlayer(audioSource);
        }

        void OnEnable()
        {
            LevelManager.OnStateChanged += OnLevelManagerStateChanged;
        }

        void OnDisable()
        { 
            LevelManager.OnStateChanged -= OnLevelManagerStateChanged;
        }

        private void OnLevelManagerStateChanged(LevelManager.State state)
        {
            if (state == LevelManager.State.PLAYING || state == LevelManager.State.FINISHED)
            {
                if (!audioPlayer.playing)
                {
                    AudioManager.PlayIfPossible(audioPlayer, AudioType.MUSIC);

                }
            }
            else
            {
                AudioManager.StopIfSound(audioPlayer,AudioType.MUSIC);
            }
        }

        void Start()
        {
            AudioManager.PlayIfPossible(audioPlayer, AudioType.MUSIC);
        }
    }

    public interface IStadium
    {
        IEnumerable<ThrowPoint> leftThrowPoints { get; }
        IEnumerable<ThrowPoint> rightThrowPoints { get; }
    }
}