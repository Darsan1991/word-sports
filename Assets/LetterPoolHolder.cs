﻿using System;
using UnityEngine;

public class LetterPoolHolder : MonoBehaviour,ILetterPoolHolder
{
    public event Action<IUnityEvent> OnEnableEvent;
    public event Action<IUnityEvent> OnDisableEvent;

    public event Action<IRectable> OnSizeChanged;

    public Vector2 size
    {
        get
        {
            return _size;
        }
        set
        {
            if (_size != value)
            {
//                Debug.Log("Level Pool Holder set size:");
                _size = value;
                OnSizeChanged?.Invoke(this);
            }

        }
    }

    public Vector2 pivot => new Vector2(0.5f,0.5f);

    private Vector2 _size = new Vector2(0.5f,0.5f);

    void OnEnable()
    {
        OnEnableEvent?.Invoke(this);
    }


    void OnDisable()
    {
        OnDisableEvent?.Invoke(this);
    }
}