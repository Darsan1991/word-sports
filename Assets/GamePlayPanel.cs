﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class GamePlayPanel : MonoBehaviour,IStartUpResolve
    {
        public const float MAX_TIME_TO_START_BEEP = 10f;

        [SerializeField] private Button blitzBtn, hintBtn,letterBtn;
        [SerializeField] private HintPanel hintPanel;
        [SerializeField] private Text timeTxt;
        [SerializeField] private GameObject[] activateObjectsOnGameStart;
        [SerializeField] private AudioClip turnStartClip,timerBeepClip,hintShowHideClip;



        public bool exposedBlitz
        {
            get { return !blitzBtn.interactable; }
            private set { blitzBtn.interactable = !value; }
        }

        public bool showingHint
        {
            get { return !hintBtn.interactable; }
            private set { hintBtn.interactable = !value; }
        }

        private int letterCount
        {
            get
            {
                return _letterCount;
            }
            set
            {
               
                _letterCount = value;
                letterBtn.GetComponentInChildren<Text>().text = "Letter("+letterCount+")";

            }
        }

        private int hintCount
        {
            get
            {
                return _hintCount;
            }
            set
            {
                _hintCount = value;
                hintBtn.GetComponentInChildren<Text>().text = "Hint(" + hintCount + ")";
            }
        }

        private int blitzCount
        {
            get
            {
                return _blitzCount;
            }
            set
            {
                _blitzCount = value;
                blitzBtn.GetComponentInChildren<Text>().text = "Blitz(" + blitzCount + ")";
            }
        }

        private int remainTime
        {
            get { return _remainTime; }
            set
            {
                if(_remainTime==value)
                    return;
                _remainTime = value;
                var minutes = remainTime / 60;
                var seconds = remainTime % 60;
                timeTxt.text = (minutes < 10 ? ("0" + minutes) : (minutes + "")) + ":" +
                               (seconds < 10 ? ("0" + seconds) : (seconds + ""));
                if(remainTime<=MAX_TIME_TO_START_BEEP)
                {
                    if (timerBeepClip!=null && remainTime!=0)
                        AudioManager.PlayIfPossible(timerBeepClip);
                    StartCoroutine(TimerTextLerpColor(Color.red, 5f,OnFinished: () =>
                    {
                        if(remainTime!=0)
                        StartCoroutine(TimerTextLerpColor(Color.white, 3f, delay: 0.2f));
                    }));
                }
            }
        }

        private PopUpPanel popUpPanel => SharedUIManager.popUpPanel;


        LevelManager levelManager;
        private int _letterCount;
        private int _hintCount;
        private int _blitzCount;
        private int _remainTime;


        void OnEnable()
        {
           LateCall.Create().Call(()=>LevelManager.instance!=null, () =>
           {
               levelManager = LevelManager.instance;
               OnPlayTimeUpdated(levelManager.playedTime);
               levelManager.OnPlayTimeUpdated += OnPlayTimeUpdated;
               
           });

            letterCount = PrefManager.GetConsumableVal(PrefManager.LETTER_TYPE);
            hintCount = PrefManager.GetConsumableVal(PrefManager.HINT_TYPE);
            blitzCount = PrefManager.GetConsumableVal(PrefManager.BLITZ_TYPE);
            PrefManager.OnConsumableValUpdated += OnConsumableValUpdated;
        }


        void OnDisable()
        {
            if (levelManager!=null)
            {
            levelManager.OnPlayTimeUpdated -= OnPlayTimeUpdated;

            }
            PrefManager.OnConsumableValUpdated -= OnConsumableValUpdated;

        }




        private void OnConsumableValUpdated(Type type, int val)
        {
            if (type == PrefManager.HINT_TYPE)
            {
                hintCount = val;
            }
            else if(type == PrefManager.BLITZ_TYPE)
            {
                blitzCount = val;
            }
            else if (type == PrefManager.LETTER_TYPE)
            {
                letterCount = val;
            }
        }

        private void OnPlayTimeUpdated(float time)
        {

            remainTime = Mathf.Clamp(
                Mathf.CeilToInt(LevelManager.MAX_GAME_PLAY_TIME - time)
                , 0
                , int.MaxValue);
            
        }

        public void OnClickBlitz()
        {
            if (blitzCount <= 0)
            {
                ShowAlert("Not Enough Blitzes!", "Do you want to open the store to buy Blitzes?");
                return;
            }
            if (!exposedBlitz)
            {
                if (levelManager.Blitz())
                {
                    PrefManager.SubtractConsumableVal(PrefManager.BLITZ_TYPE, 1);
                    exposedBlitz = true;
                }
               
            }
        }

        public void OnClickHint()
        {
            if (hintCount <= 0)
            {
                ShowAlert("Not Enough Hints!", "Do you want to open the store to buy Hints?");
                return;
            }

            if (!showingHint)
            {
                if(hintShowHideClip!=null)
                AudioManager.PlayIfPossible(hintShowHideClip);
                showingHint = true;
                hintPanel.hint = levelManager.currentQuiz.hint;
                hintPanel.Show();
                PrefManager.SubtractConsumableVal(PrefManager.HINT_TYPE, 1);
                var targetTime = Time.time + HintPanel.HINT_SHOW_TIME;
                LateCall.Create().Call(() => targetTime < Time.time, () =>
                {
                    if (levelManager.currentState == LevelManager.State.PLAYING)
                    {
                        hintPanel.Hide(() => showingHint = false);
                        if (hintShowHideClip != null)
                            AudioManager.PlayIfPossible(hintShowHideClip);
                    }
                });

            }

          
        }

        public void OnClickLetterButton()
        {
            if (letterCount <= 0)
            {
               ShowAlert("Not Enough Letters!","Do you want to open the store to buy Letters?");
                return;
            }
            if (levelManager.ExposedLetter())
            {
                PrefManager.SubtractConsumableVal(PrefManager.LETTER_TYPE, 1);
            }
        }

        void ShowAlert(string title, string message)
        {
           
            popUpPanel.viewModel = new PopUpPanel.ViewModel
            {
                title = title,
                message = message,
                buttons = new List<PopUpPanel.ViewModel.Button>
                {
                    new PopUpPanel.ViewModel.Button
                    {
                        title = "No"
                    },
                    new PopUpPanel.ViewModel.Button
                    {
                        title = "Yes",
                        OnClick = () =>
                        {
                            levelManager.PauseTheGame();
                            var storePanel = SharedUIManager.storePanel;
                            Action OnFinished = null;
                            OnFinished = () =>
                            {
                                storePanel.OnHide -= OnFinished;
                                levelManager.ResumeTheGame();
                            };
                            storePanel.OnHide += OnFinished;
                            storePanel.Show();
                        }
                    }
                }
            };
            popUpPanel.active = true;
        }

        public void Resolve(Action OnComplete = null)
        {
            if(turnStartClip!=null)
                AudioManager.PlayIfPossible(turnStartClip);
            if (activateObjectsOnGameStart != null)
            {
                foreach (var go in activateObjectsOnGameStart)
                {
                    go.SetActive(true);
                }
            }
            
            var targetTime = Time.time+0.5f;
            LateCall.Create().Call(()=>targetTime<Time.time,OnComplete);
        }

        public void OnClickPauseBtn()
        {
            levelManager.PauseTheGame();
            var uiManager = UIManager.instance;
            uiManager.pausePanel.Show();
        }

        IEnumerator TimerTextLerpColor(Color targetColor,float speed=1f,Action OnFinished=null,float delay=0f)
        {
            yield return new WaitForSeconds(delay);
            var startColor = timeTxt.color;
            var currentNormalized = 0f;
            while (currentNormalized<1f)
            {
                currentNormalized = Mathf.MoveTowards(currentNormalized, 1, Time.deltaTime * speed);
                timeTxt.color = Color.Lerp(startColor, targetColor, currentNormalized);
                yield return null;
            }
            OnFinished?.Invoke();
        }
    }
}