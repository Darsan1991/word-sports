﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class GridSizableLayout : SizableLayout
{

    [SerializeField] private Measurement widthMeasurementSer;
    [SerializeField] private Measurement heightMeasurementSer;
    [SerializeField] private float horizontalSpaceSer;
    [SerializeField] private float verticalSpaceSer;
    [SerializeField] private bool fixedColumnSer;
    [SerializeField] private int fixedColumnCountSer;
    [SerializeField] private int fixedRowCountSer;
    [SerializeField] private Offset offsetSer;

    #region Properties

    public Measurement widthMeasurement
    {
        get { return _widthMeasurement; }
        set
        {
            _widthMeasurement = value;
            UpdateChildLayout();
        }
    }

    public Measurement heightMeasurement
    {
        get { return _heightMeasurement; }
        set
        {
            _heightMeasurement = value;
            UpdateChildLayout();
        }
    }

    public float horizontalSpace
    {
        get { return _horizontalSpace; }
        set
        {
            _horizontalSpace = value;
            UpdateChildLayout();
        }
    }

    public float verticalSpace
    {
        get { return _verticalSpace; }
        set
        {
            _verticalSpace = value;
            UpdateChildLayout();
        }
    }

    public Offset offset
    {
        get { return _offset; }
        set
        {
            if (_offset != value)
            {
                _offset = value;
                UpdateChildLayout();
            }
        }
    }

    public int fixedColumnCount
    {
        get { return _fixedColumnCount; }
        set
        {
            _fixedColumnCount = value;
            UpdateChildLayout();
            fixedColumnCountSer = fixedColumnCount;
        }
    }

    public bool fixedColumn
    {
        get { return _fixedColumn; }
        set
        {
            _fixedColumn = value;
            UpdateChildLayout();
        }
    }

    public int fixedRowCount
    {
        get { return _fixedRowCount; }
        set
        {
            _fixedRowCount = value; 
            UpdateChildLayout();
        }
    }

    private float tileWidth => widthMeasurement.lengthType != LengthType.AUTO
        ? (width - offset.left - offset.right - (columnCount - 1) * horizontalSpace) / columnCount
        : (tileList.Count > 0 ? tileHeight * tileList[0].size.x / tileList[0].size.y : tileHeight);

    private float tileHeight => heightMeasurement.lengthType != LengthType.AUTO
        ? (height - offset.top - offset.bottom - (rowCount - 1) * verticalSpace) / rowCount
        : (tileWidth * (tileList.Count > 0 ? tileList[0].size.x / tileList[0].size.y : 1));

    public Vector2 tileSize => new Vector2(tileWidth, tileHeight);

    public int columnCount => fixedColumn ? fixedColumnCount : Mathf.CeilToInt((tileList.Count + 0f) / fixedRowCount);
    public int rowCount => !fixedColumn ? fixedRowCount : Mathf.CeilToInt((tileList.Count + 0f) / fixedColumnCount);

    protected override float height
    {
        get
        {
            if (heightMeasurement.lengthType == LengthType.CUSTOM_VALUE)
            {
                return heightMeasurement.length;
            }
            if (heightMeasurement.lengthType == LengthType.FILL_PARENT)
            {
                return worldRect.height;
            }
            if (heightMeasurement.lengthType == LengthType.AUTO)
            {
//                Debug.Log("tile size:"+tileSize+" row count:"+rowCount+"tile count:"+tileList.Count);
                return tileSize.y * rowCount + offset.top + offset.bottom + (rowCount - 1) * verticalSpace;
            }

            return 0;
        }
    }

    protected override float width
    {
        get
        {
            if (widthMeasurement.lengthType == LengthType.CUSTOM_VALUE)
            {
                return widthMeasurement.length;
            }
            if (widthMeasurement.lengthType == LengthType.FILL_PARENT)
            {
                return worldRect.width;
            }
            if (widthMeasurement.lengthType == LengthType.AUTO)
            {
                return tileSize.x * columnCount + offset.left + offset.right;
            }
            return 0;
        }
    }

    private Rect worldRect
    {
        get
        { 
            var camera =Camera.main;
            var lowerLeft = camera.ScreenToWorldPoint(Vector3.zero);
            var upperRight = camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
            return new Rect(lowerLeft.x, lowerLeft.y, upperRight.x - lowerLeft.x, upperRight.y - lowerLeft.y);
        }
    }

    #endregion

    #region Fields

    private float _horizontalSpace;
    private float _verticalSpace;
    private Measurement _widthMeasurement;
    private Measurement _heightMeasurement;
    private int _fixedColumnCount;
    private int _fixedRowCount;
    private bool _fixedColumn;
    private Offset _offset;

    #endregion


    protected override void UpdateChildLayout()
    {
        var startPosition = GetStartPositionForAnchor(alignment);
        var rCount = rowCount;
        var cCount = columnCount;

        var tWidth = tileSize.x;
        var tHeight = tileSize.y;

        for (var i = 0; i < rCount; i++)
        {
            for (var j = 0; j < cCount; j++)
            {
                var index = i * cCount + j;


                var pos = startPosition + Vector2.right * (tWidth * j + (j) * horizontalSpace) -
                          Vector2.up * (i * tHeight + (i) * verticalSpace);
                tileList[index].size = new Vector2(tWidth, tHeight);
                RectUtils.SetPositionWithPivot(tileList[index], pos);
                if (index >= tileList.Count - 1)
                {
                    break;
                }
            }
        }
    }

//    protected override void InitProperties()
//    {
//        base.InitProperties();
//        _widthMeasurement = widthMeasurementSer;
//        _heightMeasurement = heightMeasurementSer;
//        _horizontalSpace = horizontalSpaceSer;
//        _verticalSpace = verticalSpaceSer;
//        _offset = offsetSer;
//        _fixedColumn = fixedColumnSer;
//        _fixedColumnCount = fixedColumnCountSer;
//        _fixedRowCount = fixedRowCountSer;
////    OnValidate();
//    }

    protected Vector2 GetStartPositionForAnchor(Alignment alignment)
    {
//        return Vector2.zero;
        var h = height;
        var w = width;

        var tWidth = tileSize.x;
        var tHeight = tileSize.y;

        switch (alignment)
        {
            case Alignment.LOWER_LEFT:
                Debug.Log("y:" + (h + transform.position.y));
                return new Vector2(offset.left + tWidth / 2, h - offset.top - tHeight / 2
                       ) + (Vector2) transform.position;
            case Alignment.LOWER_MIDDLE:
                return new Vector2(-w / 2 + offset.left + tWidth / 2, h - offset.top - tHeight / 2) +
                       (Vector2) transform.position;
            case Alignment.LOWER_RIGHT:
                return new Vector2(-w + offset.left + tWidth / 2, h - offset.top - tHeight / 2) +
                       (Vector2) transform.position;
            case Alignment.MIDDLE_LEFT:
                return new Vector2(offset.left + tWidth / 2, h / 2) + (Vector2) transform.position;
            case Alignment.MIDDLE_CENTER:
                return new Vector2(w / 2, h / 2) + (Vector2) transform.position;
            case Alignment.MIDDLE_RIGHT:
                return new Vector2(-w + offset.left + tWidth / 2, h / 2) + (Vector2) transform.position;

            case Alignment.UPPER_LEFT:
                return new Vector2(offset.left + tWidth / 2, -h + offset.bottom + tHeight / 2) +
                       (Vector2) transform.position;
            case Alignment.UPPER_CENTER:
                return new Vector2(w / 2, -h + offset.bottom + tHeight / 2) + (Vector2) transform.position;
            case Alignment.UPPER_RIGHT:
                return new Vector2(-w + offset.left + tWidth / 2, -h + offset.bottom + tHeight / 2) +
                       (Vector2) transform.position;
        }

        return Vector2.zero;
    }

    protected override void OnValidate()
    {
        var measurement = widthMeasurementSer;
        if (measurement.lengthType == LengthType.AUTO && heightMeasurementSer.lengthType == LengthType.AUTO)
        {
            measurement.lengthType = LengthType.FILL_PARENT;
            widthMeasurementSer = measurement;
        }

        if (heightMeasurement != heightMeasurementSer)
            heightMeasurement = heightMeasurementSer;
        if (widthMeasurement != widthMeasurementSer)
            widthMeasurement = widthMeasurementSer;

        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (horizontalSpace != horizontalSpaceSer)
        {
            horizontalSpace = horizontalSpaceSer;
        }

        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (verticalSpace != verticalSpaceSer)
        {
            verticalSpace = verticalSpaceSer;
        }

        if (offset != offsetSer)
        {
            offset = offsetSer;
        }

        if (fixedColumn != fixedColumnSer)
        {
            fixedColumn = fixedColumnSer;
        }

        if (fixedColumnCount != fixedColumnCountSer)
        {
            fixedColumnCount = fixedColumnCountSer;
        }

        if (fixedRowCount != fixedRowCountSer)
        {
            fixedRowCount = fixedRowCountSer;
        }

        base.OnValidate();
    }

    #region Inner Classes,Structs,Enums

    [Serializable]
#pragma warning disable 660,661
    public struct Offset
#pragma warning restore 660,661
    {
        public float top, right, bottom, left;

        public static bool operator ==(Offset offset1, Offset offset2)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return !(offset1.left != offset2.left
                     // ReSharper disable once CompareOfFloatsByEqualityOperator
                     || offset1.right != offset2.right
                     // ReSharper disable once CompareOfFloatsByEqualityOperator
                     || offset1.bottom != offset2.bottom ||
                     // ReSharper disable once CompareOfFloatsByEqualityOperator
                     offset1.top != offset2.top);
        }

        public static bool operator !=(Offset offset1, Offset offset2)
        {
            return !(offset1 == offset2);
        }
    }

    [Serializable]
#pragma warning disable 660,661
    public struct Measurement
#pragma warning restore 660,661
    {
        public LengthType lengthType;
        public float length;

        public static bool operator ==(Measurement measurement1, Measurement measurement2)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return measurement1.lengthType == measurement2.lengthType && measurement1.length == measurement2.length;
        }

        public static bool operator !=(Measurement measurement1, Measurement measurement2)
        {
            return !(measurement1 == measurement2);
        }
    }

    public enum LengthType
    {
        FILL_PARENT,
        CUSTOM_VALUE,
        AUTO
    }

    #endregion
}