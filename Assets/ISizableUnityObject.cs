﻿using System;

public interface IRectUnityObject : IRectable, IUnityObject
{
    
}

public interface IUnityEvent
{
    event Action<IUnityEvent> OnEnableEvent;
    event Action<IUnityEvent> OnDisableEvent;
}


public interface ILetterPoolHolder : IRectUnityObject, IUnityEvent
{
    
}