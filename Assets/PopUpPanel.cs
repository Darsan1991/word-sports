﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpPanel : MonoBehaviour
{
    [SerializeField] private Button[] buttons;
    [SerializeField] private Text titleTxt;
    [SerializeField] private Text messageTxt;

    public ViewModel viewModel
    {
        get { return _viewModel; }
        set
        {
            _viewModel = value;
            _viewModel.buttons = viewModel.buttons ??
                new List<ViewModel.Button>
                {
                    new ViewModel.Button{title = "Ok"}
                };

            if(buttons.Length<_viewModel.buttons.Count)
                throw new Exception("Too many buttons");
            titleTxt.text = viewModel.title;
            messageTxt.text = value.message;
            for (var i = 0; i < buttons.Length; i++)
            {
                if (i < _viewModel.buttons.Count)
                {
                    var j = i;
                    buttons[i].onClick.RemoveAllListeners();
                    buttons[i].onClick.AddListener(() =>
                    {
                        OnClickButton(viewModel.buttons[j].OnClick);
                    });
                    buttons[i].gameObject.SetActive(true);
                    buttons[i].GetComponentInChildren<Text>().text = _viewModel.buttons[i].title;
                    buttons[i].image.color = _viewModel.buttons[i].color ?? buttons[i].image.color;
                }
                else
                {
                    buttons[i].gameObject.SetActive(false);
                }
            }
        }
    }

    private ViewModel _viewModel;

    public bool active { get { return gameObject.activeSelf; } set { gameObject.SetActive(value);} }

    void OnClickButton(Action Callback=null)
    {
        gameObject.SetActive(false);
        Callback?.Invoke();
    }


    public struct ViewModel
    {
        public string title { get; set; }
        public string message { get; set; }
        public List<Button> buttons { get; set; }

        public struct Button
        {
            public string title { get; set; }
            public Action OnClick { get; set; }
            public Color? color;
        }
    }
}
