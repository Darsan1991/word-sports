﻿using UnityEngine;

public class LoadingPanel : MonoBehaviour
{
    public bool showing
    {
        get
        {
            return gameObject.activeSelf;
        }
        set
        {
            gameObject.SetActive(value);
        }
    }

    public void Show()
    {
        showing = true;
    }

    public void Hide()
    {
        showing = false;
    }
}