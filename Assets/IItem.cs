﻿using System;
using UnityEngine;

public interface IItem
{
    string name { get; }
    Sprite icon { get; }
    string id { get; }
}

namespace Shop
{
    public interface IInAppItem : IItem
    {
        float price { get; }
        string productId { get; }
    }

    public interface IFeatureItem : IInAppItem
    {
        FeatureItemType featureItemType { get; }
    }

    public interface IConsumableItem : IItem
    {
        ConsumeType consumeType { get; }
    }

    public interface IConsumableValItem : IConsumableItem
    {
        int value { get; }
    }

    public interface ICoinConsumableItem : IConsumableItem
    {
        int coins { get; }
    }

    public interface ICoinConsumableValItem : IConsumableValItem, ICoinConsumableItem
    {
        
    }

    public interface IPremiumConsumableItem : IConsumableItem,IInAppItem
    {
        
    }

    public interface IPremiumConsumableValItem : IPremiumConsumableItem, IConsumableValItem
    {

    }

    public enum ConsumeType
    {
        COIN,
        PREMIUM
    }

    public interface IHintItem : IItem
    {
        int hints { get; }
    }

    public interface ILetterItem : IItem
    {
        int letters { get; }
    }

    public interface IBlitzItem : IItem
    {
        int blitzes { get; }
    }

    public interface ICoinItem : IItem
    {
        int coins { get; }
    }

    [Serializable]
    public class HintItemOfCoin : CoinConsumableValItem,IHintItem
    {
#pragma warning disable 649
        [SerializeField] private int _hints;
#pragma warning restore 649

        public int hints => _hints;
        public override int value => hints;
    }

    [Serializable]
    public class BlitzItemOfCoin : CoinConsumableValItem,IBlitzItem
    {
#pragma warning disable 649
        [SerializeField] private int _blitzes;
#pragma warning restore 649

        public int blitzes => _blitzes;
        public override int value => blitzes;
    }

    [Serializable]
    public class LetterItemOfCoin : CoinConsumableValItem, ILetterItem
    {
#pragma warning disable 649
        [SerializeField] private int _letters;
#pragma warning restore 649

        public int letters => _letters;
        public override int value =>letters;
    }



    [Serializable]
    public abstract class CoinConsumableValItem : ICoinConsumableValItem
    {
#pragma warning disable 649
        [SerializeField] private string _name;
        [SerializeField] private string _id;
        [SerializeField] private int _coins;
        [SerializeField] private Sprite _icon;
#pragma warning restore 649


        public string name => _name;

        public string id => _id;

        public Sprite icon => _icon;
        public ConsumeType consumeType => ConsumeType.COIN;

        public int coins => _coins;
        public abstract int value { get; }
    }

    [Serializable]
    public abstract class PremiumConsumableValItem : IPremiumConsumableValItem,IPremiumConsumableItem
    {
#pragma warning disable 649
        [SerializeField] private string _name;
        [SerializeField] private float _price;
        [SerializeField] private string _productId;
        [SerializeField] private Sprite _icon;
#pragma warning restore 649

        public string name => _name;

        public string id => productId;

        public ConsumeType consumeType => ConsumeType.PREMIUM;

        public Sprite icon => _icon;

        public float price => _price;

        public string productId => _productId;
        public abstract int value { get; }
    }

    [Serializable]
    public class CoinItemOfPremium : PremiumConsumableValItem,ICoinItem
    {
#pragma warning disable 649
        [SerializeField] private int _coins;
#pragma warning restore 649

        public int coins => _coins;
        public override int value => coins;
    }

    [Serializable]
    public class Item : IItem
    {
        [SerializeField] protected string _name;
        [SerializeField] protected string _id;
        [SerializeField] protected Sprite _icon;

        public virtual string name => _name;
        public virtual Sprite icon => _icon;
        public virtual string id => _id;
    }

    [Serializable]
    public class FeatureItem : Item,IFeatureItem
    {
        [SerializeField] protected float _price;
        [SerializeField] protected string _productId;
        [SerializeField] protected FeatureItemType _featureItemType;

        public float price => _price;
        public string productId => _productId;
        public FeatureItemType featureItemType => _featureItemType;
        public override string id => productId;
    }

    public enum FeatureItemType
    {
        REMOVE_ADS
    }
}