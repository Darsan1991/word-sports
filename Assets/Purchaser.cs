using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchaser : IStoreListener
{
    /// <summary>
    /// Item Purchase Completed with id and success state
    /// </summary>
    public event Action<string,bool> OnItemPurchased;

    public bool inilized => m_StoreController != null && m_StoreExtensionProvider != null;

    public IEnumerable<string> consumableItems { get; }
    public IEnumerable<string> nonConsumableItems { get;  }


    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.


    public Purchaser(IEnumerable<string> consumableItems, IEnumerable<string> nonConsumableItems)
    {
        this.nonConsumableItems = nonConsumableItems;
        this.consumableItems = consumableItems;
        Init();
    }


    private void Init()
    {
        if (inilized)
        {
            return;
        }
        if (m_StoreController == null)
        {
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            foreach (var premiumItem in consumableItems)
            {
                builder.AddProduct(premiumItem, ProductType.Consumable);
            }
            foreach (var nonConsumableItem in nonConsumableItems)
            {
                builder.AddProduct(nonConsumableItem, ProductType.NonConsumable);
            }
            UnityPurchasing.Initialize(this, builder);

        }
    }


    /// <summary>
    /// Buy the product with item
    /// </summary>
    /// <param name="productId">Product Id</param>
    /// <param name="callback">Buy product call back with success state</param>
    public void BuyProduct(string productId, Action<bool> callback)
    {
        Action<string,bool> OnPurchase = null;
        OnPurchase = (id, success) =>
        {
            if(id!=productId)
                return;
            OnItemPurchased -= OnPurchase;
            callback?.Invoke(success);
        };
        OnItemPurchased += OnPurchase;

        BuyProductID(productId);
    }


    /// <summary>
    /// The Item Already Brought for Non consumable Products
    /// </summary>
    /// <param name="productId">product id</param>
    /// <returns></returns>
    public bool ItemAlreadyPurchased(string productId)
    {
        var product = GetProduct(productId);
        if (product != null && product.definition.type != ProductType.Consumable && product.hasReceipt)
        {
            return true;
        }

        return false;
    }

    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (inilized)
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                OnItemPurchased?.Invoke(productId,false);
            }
        }
        else
        {
            OnItemPurchased?.Invoke(productId,false);
            Init();
        }
    }


    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    public Product GetProduct(string id)
    {
        return m_StoreController.products.WithID(id);
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInilized Failed");
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("Purchased Product:" + args.purchasedProduct.definition.id);
        OnItemPurchased?.Invoke(args.purchasedProduct.definition.id,true);
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) =>
        OnItemPurchased?.Invoke(product.definition.id,false);

}