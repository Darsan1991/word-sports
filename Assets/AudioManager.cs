using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance { get; private set; }
    public static AudioClip ON_CLICK_CLIP => instance.clickClip;
    [SerializeField] private AudioClip clickClip;
    [SerializeField] private AudioSource gamePlayAudioSource;

    public static bool isSound => PrefManager.GetToggleSetting(ToggleSetting.SOUND);
    public static bool isMusic => PrefManager.GetToggleSetting(ToggleSetting.MUSIC);

    private readonly List<IAudioController> audioControllers = new List<IAudioController>();

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        GameManager.OnGameStart  += OnGameStart;
        GameManager.OnGameEndOrTerminate  += OnGameTerminate;
        PrefManager.OnToggleSettingChanged += OnToggleSettingChanged;
        LevelManager.OnStateChanged += OnLevelManagerStateChanged;
    }

    void OnDisable()
    {
        GameManager.OnGameStart -= OnGameStart;
        GameManager.OnGameEndOrTerminate -= OnGameTerminate;
        PrefManager.OnToggleSettingChanged -= OnToggleSettingChanged;
        LevelManager.OnStateChanged -= OnLevelManagerStateChanged;

    }

    private void OnLevelManagerStateChanged(LevelManager.State state)
    {
        if (state == LevelManager.State.PAUSED && (gamePlayAudioSource?.isPlaying??false))
        {
            gamePlayAudioSource.Pause();
            return;
        }

        if (!gamePlayAudioSource.isPlaying && state == LevelManager.State.PLAYING)
        {
            if (isMusic)
            {
                gamePlayAudioSource.UnPause();
            }
        }
    }

    private void OnToggleSettingChanged(ToggleSetting setting, bool value)
    {
        
        if(setting!=ToggleSetting.MUSIC)
            return;
        if (value && !gamePlayAudioSource.isPlaying 
            &&(GameManager.GameHandler?.playing??false)
            &&(LevelManager.instance.currentState == LevelManager.State.PLAYING)
            )
        {
             gamePlayAudioSource.Play();
        }
        else if(!value && gamePlayAudioSource.isPlaying)
        {
            gamePlayAudioSource.Stop();
        }
    }

    private void OnGameTerminate()
    {
        if(gamePlayAudioSource.isPlaying)
        gamePlayAudioSource.Stop();
    }

    private void OnGameStart()
    {
        if (isMusic)
        {
            gamePlayAudioSource.Stop();
            gamePlayAudioSource.Play();
        }
    }


    public static void PlayIfPossible(IAudioController audioController,AudioType audioType = AudioType.SOUND)
    {
        if (!isSound && audioType == AudioType.SOUND)
            return;
        if (!isMusic && audioType == AudioType.MUSIC)
            return;
        var controllers = instance.audioControllers;
        if (!controllers.Contains(audioController))
        {
            controllers.Add(audioController);
        }
        audioController.Play();

    }

    public static void StopIfSound(IAudioController audioController,AudioType audioType=AudioType.SOUND)
    {
        if (!isSound && audioType == AudioType.SOUND)
            return;

        if (!isMusic && audioType == AudioType.MUSIC)
            return;
        var controllers = instance.audioControllers;
        if (controllers.Contains(audioController))
        {
            controllers.Remove(audioController);
            audioController.Stop();
        }
    }


    public static void PlayIfPossible(AudioClip clip)
    {
        if (isSound)
        {
            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
        }
    }
}

public enum AudioType
{
    MUSIC,SOUND
}