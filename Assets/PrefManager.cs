using System;
using System.CodeDom;
using System.Collections.Generic;
using Shop;
using UnityEngine;

public static class PrefManager
{
    public static event Action<int> OnBestScoreChanged;
    public static event Action<Type, int> OnConsumableValUpdated;
    public static event Action<ToggleSetting,bool> OnToggleSettingChanged;
    public static event Action<bool> OnPremiumStateChanged;
    public static readonly Type HINT_TYPE = typeof(IHintItem);
    public static readonly Type LETTER_TYPE = typeof(ILetterItem);
    public static readonly Type BLITZ_TYPE = typeof(IBlitzItem);
    public static readonly Type COINS_TYPE = typeof(ICoinItem);

    private const string HIGH_SCORE = "highscore";
    private const string COINS = "coins";
    private const string QUIZ = "quizzes";
    private const string CONSUMABLE_PREFIX = "consumable_";
    private const string SETTING_PREFIX = "setting_";
    private const string PREMIUM = "premium";

    public static int GetHighScore() => PlayerPrefs.GetInt(HIGH_SCORE, 0);

    public static void SetHighScore(int score)
    {
        if(score<=GetHighScore())
            return;
        PlayerPrefs.SetInt(HIGH_SCORE, score);
        OnBestScoreChanged?.Invoke(score);
    }

    public static int GetConsumableVal<T>() where T : IItem
    {
         string key = CONSUMABLE_PREFIX+ typeof(T).Name;
        Debug.Log("key:"+key);
        return PlayerPrefs.GetInt(key, 0);
    }

    public static int GetConsumableVal(Type t)
    {
//        Debug.Log("nameof:"+nameof(ICoinItem)+" type name:"+typeof(ICoinItem).Name);
//        Debug.Log((typeof(IItem)).IsAssignableFrom(typeof(IHintItem)));
        if (!(typeof(IItem)).IsAssignableFrom(t))
            throw new Exception("The Type should be type of item");
         string key = CONSUMABLE_PREFIX + t.Name;
        return PlayerPrefs.GetInt(key, 0);
    }

    public static void SetConsumableVal<T>(int val) where T : IItem
    {
        string key = CONSUMABLE_PREFIX + typeof(T).Name;
        PlayerPrefs.SetInt(key, val);
        OnConsumableValUpdated?.Invoke(typeof(T),val);
    }

    public static void SetConsumableVal(Type type,int val)
    {
        if (!(typeof(IItem)).IsAssignableFrom(type))
            throw new Exception("The Type should be type of item");
         string key = CONSUMABLE_PREFIX + type.Name;
        Debug.Log("name of type:"+type.Name);
        PlayerPrefs.SetInt(key, val);
        OnConsumableValUpdated?.Invoke(type, val);

    }

    public static void AddConsumableVal<T>(int val) where T : IItem
    {
        SetConsumableVal(typeof(T), GetConsumableVal<T>()+val);
    }

    public static void AddConsumableVal(Type type,int val) 
    {
       SetConsumableVal(type, GetConsumableVal(type) + val);

    }

    public static bool SubtractConsumableVal<T>(int val) where T : IItem
    {

        var consumableVal = GetConsumableVal<T>();
        if (consumableVal >= val)
        {
            SetConsumableVal<T>(consumableVal-val);
            return true;
        }
        return false;
    }

    public static bool SubtractConsumableVal(Type t,int val)
    {
       
        var consumableVal = GetConsumableVal(t);
        if (consumableVal >= val)
        {
            SetConsumableVal(t,consumableVal - val);
            return true;
        }
        return false;
    }

    public static int GetCoins() => PlayerPrefs.GetInt(COINS, 0);

    public static void SetCoins(int coins) => PlayerPrefs.SetInt(COINS, coins);
    public static void AddCoins(int coins) => PlayerPrefs.SetInt(COINS, GetCoins()+coins);

    public static IEnumerable<Model.IQuiz> GetQuizzes() =>
        DataUtils.QuizUtils.FromJson(PlayerPrefs.GetString(QUIZ, ""));

    public static void SetQuizzes(IEnumerable<Model.IQuiz> quizzes) =>
        PlayerPrefs.SetString(QUIZ, DataUtils.QuizUtils.ToJson(quizzes));

    public static bool GetToggleSetting(ToggleSetting setting)
    {
        return PlayerPrefs.GetInt(SETTING_PREFIX + setting, 1) == 1;
    }

    public static void SetToggleSetting(ToggleSetting setting, bool active)
    {
        if(GetToggleSetting(setting)==active)
            return;
        PlayerPrefs.SetInt(SETTING_PREFIX+setting,active?1:0);
        OnToggleSettingChanged?.Invoke(setting,active);
    }

    public static bool IsPremiumVersion() => PlayerPrefs.GetInt(PREMIUM, 0) == 1;
    public static void SetPremiumVersion(bool isPremium)
    {
        if(IsPremiumVersion()==isPremium)
            return;
        PlayerPrefs.SetInt(PREMIUM, isPremium ? 1 : 0);
        OnPremiumStateChanged?.Invoke(isPremium);
    }

    public static void SetInt(string key, int val)
    {
        PlayerPrefs.SetInt(key,val);
    }

    public static void SetString(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }

    public static void SetFloat(string key, float val)
    {
        PlayerPrefs.SetFloat(key, val);
    }

    public static void SetBool(string key, bool val) => PlayerPrefs.SetInt(key, val ? 1 : 0);
    public static bool GetBool(string key, bool d) => PlayerPrefs.GetInt(key,d?1:0)==1;

    public static void GetInt(string key, int d) => PlayerPrefs.GetInt(key, d);
    public static void GetString(string key, string d) => PlayerPrefs.GetString(key, d);
    public static void GetFloat(string key, float d) => PlayerPrefs.GetFloat(key, d);
}

public enum ToggleSetting
{
    MUSIC,SOUND,VIBRATION
}