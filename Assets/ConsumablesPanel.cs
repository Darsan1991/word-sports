using System;
using System.Collections.Generic;
using System.Linq;
using Shop;
using UnityEngine;
using UnityEngine.UI;

public class ConsumablesPanel : MonoBehaviour,IInitializable,IConsumableTileListener
{
    [SerializeField] private Text titleValTxt;
    [SerializeField] private ConsumeableTileUI tilePrefab;
    [SerializeField] private List<ConsumeableTileUI> tilePool;
    [SerializeField] private RectTransform contentPanel;
    [SerializeField] private ConsumableProductType consumableProductType;

    public bool inilized { get; private set; }

    
//    private IConsumableProvider consumableProvider => _consumableProvider as IConsumableProvider;

    private int currentVal
    {
        get { return _currentVal; }
        set
        {
            _currentVal = value;
            titleValTxt.text = currentVal.ToString();
        }
    }

    private readonly List<ConsumeableTileUI> consumableTileUIList = new List<ConsumeableTileUI>();
    private int _currentVal;




    void OnDestroy()
    {
        PrefManager.OnConsumableValUpdated -= OnConsumeValUpdated;

    }

    void Start()
    {
        Init();
    }

    private void OnConsumeValUpdated(Type type, int val)
    {
        if (GetConsumableProductType(type) == consumableProductType)
        {
            currentVal = val;
        }
    }

    public void Init()
    {
        if (inilized)
        {
            return;
        }

        var store = ResourceManager.Store;
        List<IConsumableItem> consumableItems = store
            .GetItem(GetTypeForConsumableProductType(consumableProductType))
            .OfType<IConsumableItem>().ToList();

        PrefManager.OnConsumableValUpdated += OnConsumeValUpdated;

        currentVal = PrefManager.GetConsumableVal(GetTypeForConsumableProductType(consumableProductType));

        while (consumableItems.Count>tilePool.Count)
        {
            var consumeableTileUi = Instantiate(tilePrefab);
            consumeableTileUi.transform.parent = contentPanel;
            consumeableTileUi.transform.localScale = Vector3.one;
            tilePool.Add(consumeableTileUi);
        }

        for (var i = 0; i < tilePool.Count; i++)
        {
            if (consumableItems.Count > i)
            {
                var consumeableTileUi = tilePool[i];
                consumeableTileUi.viewModel = new ConsumeableTileUI.ViewModel
                {
                    consumableItem = consumableItems[i],
                    listener = this
                };
                consumeableTileUi.gameObject.SetActive(true);
            }
            else
            {
                tilePool[i].gameObject.SetActive(false);
            }

        }


        inilized = true;
    }

    public void OnTileClicked(IConsumableTile consumableTile)
    {
        Debug.Log(nameof(OnTileClicked));
        var consumableItem = consumableTile.consumableItem;
        ResourceManager.ConsumeOrUnlockItem(consumableItem,((item, success) =>
        {
            Debug.Log("Consumable Product type:"+ GetConsumableProductType(item));
            if(!success)
                return;
            var productType = GetConsumableProductType(item) ?? consumableProductType;
            PrefManager.AddConsumableVal(GetTypeForConsumableProductType(productType),((IConsumableValItem) item).value);
        }));
    }

    //    void OnValidate()
    //    {
    //        if (!(_consumableProvider is IConsumableProvider))
    //        {
    //            _consumableProvider = null;
    //        }
    //    }

    private ConsumableProductType? GetConsumableProductType(IItem obj)
    {
        if (obj is IHintItem)
            return ConsumableProductType.HINT;
        if (obj is ILetterItem)
            return ConsumableProductType.LETTER;
        if (obj is IBlitzItem)
            return ConsumableProductType.BLITZ;
        if (obj is ICoinItem)
            return ConsumableProductType.COIN;

        return null;
    }

    private ConsumableProductType? GetConsumableProductType(Type type)
    {
        if (typeof(IHintItem).IsAssignableFrom(type))
            return ConsumableProductType.HINT;
        if (typeof(ILetterItem).IsAssignableFrom(type))
            return ConsumableProductType.LETTER;
        if (typeof(IBlitzItem).IsAssignableFrom(type))
            return ConsumableProductType.BLITZ;
        if (typeof(ICoinItem).IsAssignableFrom(type))
            return ConsumableProductType.COIN;

        return null;
    }


    private Type GetTypeForConsumableProductType(ConsumableProductType productType)
    {
        switch (productType)
        {
            case ConsumableProductType.HINT:
                return typeof(IHintItem);
            case ConsumableProductType.BLITZ:
                return typeof(IBlitzItem);
            case ConsumableProductType.LETTER:
                return typeof(ILetterItem);
            case ConsumableProductType.COIN:
                return typeof(ICoinItem);
            default:
                throw new ArgumentOutOfRangeException(nameof(productType), productType, null);
        }
    }

    public enum ConsumableProductType
    {
        HINT,
        BLITZ,
        LETTER,
        COIN
    }
}

public interface IConsumableProvider
{
    IEnumerable<IConsumableItem> consumableItems { get; }
}


