﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public abstract class SizableLayout : MonoBehaviour, IRefreshable, IRectUnityObject, IInitializable
{
    public event Action<IRectable> OnSizeChanged;


    [SerializeField] protected Alignment anchor;

    [SerializeField] protected bool refreshOnChildSizeChanged;
//    [SerializeField] protected Alignment childAlignmentSer;

    protected readonly List<IRectUnityObject> tileList = new List<IRectUnityObject>();

    public bool inilized { get; private set; }

    public virtual Vector2 size
    {
        get { return new Vector2(width, height); }
        set
        {
            //future implementation :ignore for now
        }
    }

    public virtual Vector2 pivot => GetPivotForAlignment(alignment);

    protected abstract float height { get; }

    protected abstract float width { get; }

    public virtual Alignment alignment
    {
        get { return _alignment; }
        set
        {
            _alignment = value;
            UpdateChildLayout();
        }
    }

//    public virtual Alignment childAlignment
//    {
//        get { return _childAlignment; }
//        set
//        {
//            if (_childAlignment != value)
//            {
//                _childAlignment = value;
//                UpdateChildLayout();
//            }
//        }
//    }

    private Alignment _alignment;
//    private Alignment _childAlignment;

    private void OnTileSizeChanged(IRectable rectable)
    {
        if(!refreshOnChildSizeChanged)
            return;
        OnSizeChanged?.Invoke(this);
        UpdateChildLayout();
    }

    protected virtual void Awake()
    {
//#if UNITY_EDITOR
//        InitProperties();
//        SetInilizableTiles();
////        Debug.Log("Awake");
//
//#else
        Init();
//#endif
    }


    public void Init()
    {
        if (inilized)
        {
            return;
        }
        InitProperties();
        SetInilizableTiles();
        inilized = true;
    }

    protected virtual void InitProperties()
    {
//        alignment = anchor;
OnValidate();
    }

    protected virtual void OnDrawGizmos()
    {
//        Debug.Log("Size:"+size);
        this.DrawGizmos(Color.red);
    }

    protected void SetInilizableTiles()
    {
        tileList.Clear();
//        Debug.Log("tile count:"+transform.childCount);
        var list = new List<IRectUnityObject>();
        
        foreach (Transform tr in transform)
        {
            if(tr==null || !tr.gameObject.activeSelf)
                continue;
//            Debug.Log("tr :"+tr.gameObject.name);
            var sizableUnityObject = tr.GetComponent<IRectUnityObject>();
            if (sizableUnityObject != null)
            {
                var initializable = sizableUnityObject as IInitializable;
                if (initializable != null && !initializable.inilized)
                    initializable.Init();
                list.Add(sizableUnityObject);
            }
        }
        AddChilds(list.ToArray());
        UpdateChildLayout();
    }

    public void AddChild(IRectUnityObject sizable)
    {
        tileList.Add(sizable);
        sizable.OnSizeChanged -= OnTileSizeChanged;
        sizable.OnSizeChanged += OnTileSizeChanged;
        UpdateChildLayout();
    }

    public void AddChilds(params IRectUnityObject[] sizables)
    {
        if (sizables != null)
            tileList.AddRange(sizables);
        foreach (var rectUnityObject in sizables)
        {
            if (rectUnityObject == null)
                continue;
            rectUnityObject.OnSizeChanged -= OnTileSizeChanged;
            rectUnityObject.OnSizeChanged += OnTileSizeChanged;
        }

        UpdateChildLayout();
    }

    protected abstract void UpdateChildLayout();

//    protected abstract Vector2 GetStartPositionForAlignment(Alignment alignment);

    public void Refresh()
    {
        SetInilizableTiles();
        tileList.ForEach((t) => (t as IRefreshable)?.Refresh());
        for (var i = 0; i < tileList.Count; i++)
        {
            if (tileList[i] == null)
            {
                tileList.RemoveAt(i);
                i--;
            }
        }
        SetInilizableTiles();
//        Debug.Log("Refresh:"+gameObject.name);
    }

    public static Vector2 GetPivotForAlignment(Alignment alignment)
    {
        switch (alignment)
        {
            case Alignment.LOWER_LEFT:
                return new Vector2(0, 0);
            case Alignment.LOWER_MIDDLE:
                return new Vector2(0.5f, 0);
            case Alignment.LOWER_RIGHT:
                return new Vector2(1, 0);
            case Alignment.MIDDLE_LEFT:
                return new Vector2(0, 0.5f);
            case Alignment.MIDDLE_CENTER:
                return new Vector2(0.5f, 0.5f);
            case Alignment.MIDDLE_RIGHT:
                return new Vector2(1, 0.5f);
            case Alignment.UPPER_LEFT:
                return new Vector2(0, 1);
            case Alignment.UPPER_CENTER:
                return new Vector2(0.5f, 1);
            case Alignment.UPPER_RIGHT:
                return new Vector2(1f, 1);
        }
        return Vector2.zero;
    }


    protected virtual void OnValidate()
    {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        // ReSharper disable once RedundantCheckBeforeAssignment

        if (alignment != anchor)
        {
            alignment = anchor;
        }

//        if (childAlignment != childAlignmentSer)
//        {
//            childAlignment = childAlignmentSer;
//        }
    }
}

public interface IInitializable
{
    bool inilized { get; }
    void Init();
}

public interface IInitializable<T>
{
    bool inilized { get; }
    void Init(T t);
}