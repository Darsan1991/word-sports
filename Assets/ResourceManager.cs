using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Model;
using Shop;
using UnityEngine;

public class ResourceManager : MonoBehaviour, IInitializable
{
    public const string LOAD_DATA_URL = "http://api1.petalmedia.co.uk/LoadAll.php";
    private const string PRE_DATA_FILE_NAME = "pre_data";

    public static ResourceManager instance { get; private set; }

    [SerializeField] private StadiumScriptable _stadiumScriptable;
    [SerializeField] private StoreScriptable _storeScriptable;
    [SerializeField] private TextAsset _preData;

    public static IEnumerable<Model.IStadium> Stadiums => instance.stadiumScriptable.stadiums;

    public static IEnumerable<Model.IQuiz> DefaultQuizzes
    {
        get { return PrefManager.GetQuizzes() ?? DataUtils.QuizUtils.FromJson(instance.preData.text); }
        private set { PrefManager.SetQuizzes(value); }
    }

    public static bool Inilized => instance?.inilized??false;
    public static StoreScriptable StoreScriptable => instance.storeScriptable;
    public static IStore Store => instance.storeScriptable;
    public static IEnumerable<Model.IQuiz> Quizzes => instance.quizzes;

    public Purchaser purchaser { get; private set; }

    public bool inilized { get; private set; }

    private IEnumerable<Model.IQuiz> quizzes;

    public StadiumScriptable stadiumScriptable
    {
        get
        {
            if (_stadiumScriptable == null)
                _stadiumScriptable = Resources.Load<StadiumScriptable>(StadiumScriptable.DEFAULT_FILE_NAME);
            return _stadiumScriptable;
        }
    }


    public StoreScriptable storeScriptable
    {
        get
        {
            if (_storeScriptable == null)
                _storeScriptable = Resources.Load<StoreScriptable>(StoreScriptable.DEFAULT_FILE_NAME);
            return _storeScriptable;
        }
    }

    public TextAsset preData
    {
        get
        {
            if (_preData == null)
            {
                _preData = Resources.Load<TextAsset>(PRE_DATA_FILE_NAME);
            }
            return _preData;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        Init();
    }

    public void Init()
    {
        if (inilized)
        {
            return;
        }
        var items = Store.GetAllItems().OfType<IInAppItem>();
        var inAppItems = items as IInAppItem[] ?? items.ToArray();
        var consumableItems = inAppItems.OfType<IConsumableItem>().Select((c) => (IInAppItem) c);
        var cItems = consumableItems as IInAppItem[] ?? consumableItems.ToArray();
        var nonConsumableItems = inAppItems.Except(cItems.Select((c) => (IInAppItem) c));

        purchaser = new Purchaser(cItems.Select((c) => c.productId), nonConsumableItems.Select((c) => c.productId));
        this.quizzes = DefaultQuizzes;
        StartCoroutine(LoadDataFromServer(((success, quizzes) =>
        {
            DefaultQuizzes = success ? quizzes as IQuiz[] ?? quizzes.ToArray() : DefaultQuizzes;
            this.quizzes = quizzes ?? DefaultQuizzes;
//            Debug.Log(DataUtils.QuizUtils.ToJson(defaultQuizzes));
            inilized = true;
        })));
    }

    private IEnumerator LoadDataFromServer(Action<bool, IEnumerable<Model.IQuiz>> OnCompleted)
    {
        var www = new WWW(LOAD_DATA_URL);
        yield return www;
        if (www.error != null)
        {
            OnCompleted?.Invoke(false, null);
        }
        else
        {
            var json = www.text;
            var databaseData = JsonUtility.FromJson<DatabaseData>(json);
            var dataQuizzes = databaseData.quizzes;
            OnCompleted?.Invoke(dataQuizzes != null, dataQuizzes);
        }
    }

    /// <summary>
    /// Consume The Store Item
    /// </summary>
    /// <param name="item">item</param>
    /// <param name="OnCompleted">OnCompleted callback with item and status-completed/not completed</param>
    public static void ConsumeOrUnlockItem(IItem item, Action<IItem, bool> OnCompleted = null)
    {
        var consumableItem = item as IConsumableItem;
        if (consumableItem != null)
        {
            if (consumableItem.consumeType == ConsumeType.COIN)
            {
                var coinConsumableItem = (ICoinConsumableItem) consumableItem;
                Debug.Log("Coins Consumable item current coins:"+ PrefManager.GetConsumableVal(typeof(ICoinItem)));
                if (PrefManager.GetConsumableVal<ICoinItem>() >= coinConsumableItem.coins)
                {
                    
                    PrefManager.SubtractConsumableVal<ICoinItem>(coinConsumableItem.coins);
                    OnCompleted?.Invoke(item,true);
                }
                else
                {
                    var popUpPanel = SharedUIManager.popUpPanel;
                    var storePanelShowing = SharedUIManager.storePanel.showing;
                    popUpPanel.viewModel = new PopUpPanel.ViewModel
                    {
                        title = "Not Enough Coins!",
                        message = storePanelShowing? "You may acquire more coins to obtain this Game Boost." : "Do you want to open the Store to buy the coins",
                        buttons = storePanelShowing?new List<PopUpPanel.ViewModel.Button>
                        {
                            new PopUpPanel.ViewModel.Button
                            {
                                title = "Ok"
                            }
                        }:
                        new List<PopUpPanel.ViewModel.Button>
                        {
                            new PopUpPanel.ViewModel.Button
                            {
                                title = "No"
                            },
                            new PopUpPanel.ViewModel.Button
                            {
                                title = "Yes",
                                OnClick = () =>
                                {
                                    SharedUIManager.storePanel.Show();
                                }
                            }
                        }

                    };
                    popUpPanel.active = true;
                }
            }
            else if (consumableItem.consumeType == ConsumeType.PREMIUM)
            {
                instance.purchaser.BuyProduct(((IInAppItem)item).productId,(success => OnCompleted?.Invoke(item,success)));
            }
        }
        else if (item is IInAppItem)
        {
            instance.purchaser.BuyProduct(((IInAppItem)item).productId, (success => OnCompleted?.Invoke(item, success)));
        }
    }

    public static Model.IStadium GetStadiumForSports(string sportName)
    {
        var list = Stadiums.ToList();
        return list.Find((stadium => string.Equals(stadium.sportName, sportName,
            StringComparison.CurrentCultureIgnoreCase)));
    }

    [System.Serializable]
    private struct DatabaseData
    {
#pragma warning disable 649
        // ReSharper disable once CollectionNeverUpdated.Local
        [SerializeField] private List<Quiz> data;
#pragma warning restore 649

        public IEnumerable<Model.IQuiz> quizzes => data.Select(quiz => (Model.IQuiz) quiz);

        [System.Serializable]
        private struct Quiz : Model.IQuiz
        {
#pragma warning disable 169
#pragma warning disable 649
            //the Field name in database
            [SerializeField] private string SPORT;
            [SerializeField] private string ANSWER;
            [SerializeField] private string HINT;
#pragma warning restore 169
#pragma warning restore 649


            public string sportName => SPORT;
            public string answer => ANSWER;
            public string hint => HINT;
        }
    }
}

public interface ILevelManager
{
    event Action OnLevelStart;
    event Action<bool> OnLevelEnd;
    float playedTime { get; }
}