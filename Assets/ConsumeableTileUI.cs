﻿using Shop;
using UnityEngine;
using UnityEngine.UI;

public class ConsumeableTileUI : MonoBehaviour, IConsumableTile
{
    [SerializeField] private GameObject coinsGroup;
    [SerializeField] private GameObject premiumGroup;
    [SerializeField] private Text valueTxt;
    [SerializeField] private Image img;

    public virtual ViewModel viewModel
    {
        get { return _viewModel; }
        set
        {
            _viewModel = value;
            var cItem = _viewModel.consumableItem;
            coinsGroup.SetActive(cItem.consumeType == ConsumeType.COIN);
            premiumGroup.SetActive(cItem.consumeType == ConsumeType.PREMIUM);
            img.sprite = value.consumableItem.icon;
            if (cItem.consumeType == ConsumeType.COIN)
            {
//                Debug.Log(cItem);
                coinsGroup.GetComponentInChildren<Text>().text =
                    ((ICoinConsumableItem) cItem).coins.ToString();
            }
            else
            {
                premiumGroup.GetComponentInChildren<Text>().text =
                    ((IPremiumConsumableItem) cItem).price + "$";
            }

            if (valueTxt != null)
            {
                var consumableValItem = viewModel.consumableItem as IConsumableValItem;
                valueTxt.text = consumableValItem?.value.ToShortString();
            }
        }
    }

    public IConsumableItem consumableItem => viewModel.consumableItem;

    protected ViewModel _viewModel;

    public void OnClickTile()
    {
        viewModel.listener?.OnTileClicked(this);
    }

    public struct ViewModel
    {
        public IConsumableItem consumableItem { get; set; }
        public IConsumableTileListener listener { get; set; }
    }
}

public interface IConsumableTile
{
    IConsumableItem consumableItem { get; }
}

public interface IConsumableTileListener
{
    void OnTileClicked(IConsumableTile consumableTile);
}