using System;
using System.Linq;
using GooglePlayGames.Native.Cwrapper;
using Model;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static event Action OnGameStart;
    public static event Action OnGameEndOrTerminate;
    public static GameManager instance { get; private set; }

    public static IGameHandler GameHandler => instance.gameHandler;

    private IGameHandler gameHandler
    {
        get
        {
            return _gameHandler;
        }
        set
        {
            if (_gameHandler != value)
            {
                if (_gameHandler!=null)
                {
                    _gameHandler.OnEndTheLevel -= OnEndTheLevel;
                }
                _gameHandler = value;

                if (_gameHandler != null)
                {
                    _gameHandler.OnEndTheLevel += OnEndTheLevel;
                }
            }
        }
    }

    


    private IGameHandler _gameHandler;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            Application.targetFrameRate = 60;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnEndTheLevel(bool success)
    {
        Debug.Log(nameof(OnEndTheLevel));
        if (!success)
        {
            PrefManager.AddCoins(gameHandler.currentScore);
            if(gameHandler.currentScore>PrefManager.GetHighScore())
            PrefManager.SetHighScore(gameHandler.currentScore);
            gameHandler = null;
            OnGameEndOrTerminate?.Invoke();
        }
    }

    public static void StartTheGame()
    {
       var quizzes = ResourceManager.Quizzes;
        var enumerable = quizzes as IQuiz[] ?? quizzes.ToArray();
//        DebugUIManager.Show("quiz count:"+enumerable?.Count());
        instance.gameHandler = new GameHandler(new GameResultCalculator(),enumerable);
        LoadScene("Main");
        OnGameStart?.Invoke();
    }


    public static void GoToMainMenu()
    {
        if (SceneManager.GetActiveScene().name.ToLower() == "MainMenu")
        {
            throw new Exception("You are currently in MainMenu");
        }
        if (GameHandler != null && GameHandler.playing)
        {
            OnGameEndOrTerminate?.Invoke();
        }
        LoadScene("MainMenu");
    }

    public static void Exit()
    {
        SceneManager.LoadScene("ExitingScreen");
    }

    public static void ContinueTheGame()
    {
       if(GameHandler==null)
            throw new Exception("Game still not stated");
       LoadScene("Main");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartTheGame();
        }
    }

    public static void LoadScene(string sceneName)
    {
        SharedUIManager.loadingPanel.Show();
        SceneManager.LoadScene(sceneName);
    }

    public static void LoadScene(int scene)
    {
        SharedUIManager.loadingPanel.Show();
        SceneManager.LoadScene(scene);
    }

    public struct GameResultCalculator:IGameResultCalculator
    {
        public void CalculateResult(float completedTime, int currentLevel,out int score,out int coins)
        {
            if (completedTime <= 10)
            {

                score = 150;
                coins = 15;
                return;
            }

            if (completedTime <= 20)
            {
                coins = 10;
                score = 100;
                return;
            }

            if (completedTime <= 30)
            {
                coins = 5;
                score = 50;
                return;
            }

            score = 0;
            coins = 0;
        }
    }
}