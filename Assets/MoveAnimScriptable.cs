﻿using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/MoveAnim")]
public class MoveAnimScriptable : ScriptableObject,IMoveAnim
{
    [SerializeField]private  AnimationCurve _speedAnimationCurve;
    [SerializeField]private  AnimationCurve _scaleAnimationCurve;
    [SerializeField]private  AnimationCurve _pathAnimationCurve;

    public AnimationCurve speedAnimationCurve => _speedAnimationCurve;
    public AnimationCurve scaleAnimationCurve => _scaleAnimationCurve;
    public AnimationCurve pathAnimationCurve => _pathAnimationCurve;
}