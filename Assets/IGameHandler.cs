﻿using System;

public interface IGameHandler
{
    event Action OnStartTheLevel;
    event Action<bool> OnEndTheLevel;

    int currentLevel { get; }
    int currentScore { get; }
    int currentCoins { get; }
    Model.IQuiz currentQuiz { get; }

    bool playing { get; }

    void EndTheLevel(bool success);
    void StartTheLevel(ILevelManager levelManager);
}